package com.lanteam.kubernetesapi.server;

import com.lanteam.kubernetesapi.server.controller.CatalogControllerTest;
import com.lanteam.kubernetesapi.server.controller.RestRequestValidatorSuite;
import com.lanteam.kubernetesapi.server.controller.ServiceBindingsControllerTest;
import com.lanteam.kubernetesapi.server.controller.ServiceInstancesControllerTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CatalogControllerTest.class,
        ServiceBindingsControllerTest.class,
        ServiceInstancesControllerTest.class,
        RestRequestValidatorSuite.class
})
public class CompleteTestSuite {
}
