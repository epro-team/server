package com.lanteam.kubernetesapi.server.service;

import com.lanteam.kubernetesapi.server.model.Instance;
import com.lanteam.kubernetesapi.server.util.helper.InstanceFileManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.concurrent.Semaphore;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.powermock.api.mockito.PowerMockito.*;

/**
 * tests the (async) K8sRedisService
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Jedis.class, InstanceFileManager.class, HelmService.class, K8sRedisService.class})
@SpringBootTest
public class K8SRedisServiceTest {
	private K8sRedisService     k8SRedisService;
	private Jedis               jedis               = mock(Jedis.class);
	private HelmService         helmService         = mock(HelmService.class);
	private InstanceFileManager instanceFileManager = mock(InstanceFileManager.class);

	@Before
	public void setUp() throws Exception {
		this.k8SRedisService = new K8sRedisService(helmService, instanceFileManager);

		whenNew(Jedis.class).withAnyArguments().thenReturn(jedis);
		whenNew(HelmService.class).withAnyArguments().thenReturn(helmService);
		whenNew(InstanceFileManager.class).withAnyArguments().thenReturn(instanceFileManager);
	}

	/**
	 * Tests the set parameter function
	 * <p>
	 * Because @{link K8sRedisService#setParamters} uses async methods this class uses a semaphore to check the result
	 *
	 * @throws Exception Mockito Exception
	 */
	@Test
	public void setParameters() throws Exception {
		final Semaphore semaphore = new Semaphore(0);

		final HashMap<String, String> save = new HashMap<>();
		final HashMap<String, String> map  = new HashMap<>();
		map.put("test", "test");
		final String instanceID = "test";


		Instance instance = new Instance();
		instance.setParameters(new HashMap<>());
		HashMap<String, Instance> instanceMap = new HashMap<>();
		instanceMap.put(instanceID, instance);


		when(instanceFileManager, "loadFile").thenReturn(instanceMap);
		when(instanceFileManager, "putInstance", anyString(), any()).then(invocationOnMock -> {
			semaphore.release();
			return invocationOnMock;
		});
		when(helmService, "isReady", anyString()).thenReturn(true);
		when(helmService, "getRedisUrl", anyString()).thenReturn("localhost:8080");
		when(jedis, "configSet", anyString(), anyString())
				.then(invocationOnMock -> save.put(invocationOnMock.getArgument(0), invocationOnMock.getArgument(1)));


		k8SRedisService.setParameters(instanceID, map);
		semaphore.acquire();
		assertEquals(map, save);
	}
}