package com.lanteam.kubernetesapi.server.util.helper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class HelmHelperTest {
	@Test
	public void constance() {
		assertNotNull(HelmHelper.KEYS.SLUG);
		assertTrue(HelmHelper.KEYS.SLUG instanceof String);
	}

	/**
	 * Tests if the slug add method work correct
	 */
	@Test
	public void makeSlug() {
		String instanceId = UUID.randomUUID().toString();

		assertEquals(HelmHelper.KEYS.SLUG + instanceId, HelmHelper.makeSlug(instanceId));
		assertEquals(HelmHelper.KEYS.SLUG + instanceId, HelmHelper.makeSlug(HelmHelper.KEYS.SLUG + instanceId));
	}

	/**
	 * Tests if the slug remove method work correct
	 */
	@Test
	public void removeSlug() {
		String instanceId = UUID.randomUUID().toString();
		assertEquals(instanceId, HelmHelper.removeSlug(HelmHelper.KEYS.SLUG + instanceId));
		assertEquals(instanceId, HelmHelper.removeSlug(instanceId));
	}
}