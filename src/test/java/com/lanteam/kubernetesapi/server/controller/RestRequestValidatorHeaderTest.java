package com.lanteam.kubernetesapi.server.controller;

import com.lanteam.kubernetesapi.server.exceptions.BadRequestException;
import com.lanteam.kubernetesapi.server.exceptions.UnsupportedApiLevelException;
import com.lanteam.kubernetesapi.server.model.request.GlobalRequestHeader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestRequestValidatorHeaderTest {

    @Autowired
    private RestRequestValidator restRequestValidator;

    /**
     * API Version is in a wrong format and has alphanumeric characters -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateHeaderTestApiVersionAlphanumeric() {
        GlobalRequestHeader globalRequestHeader = new GlobalRequestHeader("B.1f");
        restRequestValidator.validateHeader(globalRequestHeader);
    }

    /**
     * API Version is in a wrong format -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateHeaderTestApiVersionWrongFormat() {
        GlobalRequestHeader globalRequestHeader = new GlobalRequestHeader(".2.1.2");
        restRequestValidator.validateHeader(globalRequestHeader);
    }

    /**
     * API Version is not supported -> has to throw BadRequestException
     */
    @Test(expected = UnsupportedApiLevelException.class)
    public void validateHeaderTestApiVersionNotSupported() {
        GlobalRequestHeader globalRequestHeader = new GlobalRequestHeader("2.12");
        restRequestValidator.validateHeader(globalRequestHeader);
    }

    /**
     * API Version is supported
     */
    @Test
    public void validateHeaderTestSuccess() {
        GlobalRequestHeader globalRequestHeader = new GlobalRequestHeader("2.14");
        restRequestValidator.validateHeader(globalRequestHeader);
    }

    /**
     * X-Broker-API-Originating-Identity wrong format given -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateHeaderOriginatingIdentityWrongFormat() {
        String xOriginatingIdentity = "wrongFormat";
        GlobalRequestHeader globalRequestHeader = new GlobalRequestHeader("2.14", xOriginatingIdentity);
        restRequestValidator.validateHeader(globalRequestHeader);
    }

    /**
     * X-Broker-API-Originating-Identity wrong Platform given -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateHeaderOriginatingIdentityWrongPlatform() {
        String xOriginatingIdentity = "wrongPlatform " + Base64.getEncoder().encodeToString("{\"user_name\": \"683ea748-3092-4ff4-b656-39cacc4d5360\"}".getBytes(StandardCharsets.UTF_8));
        GlobalRequestHeader globalRequestHeader = new GlobalRequestHeader("2.14", xOriginatingIdentity);
        restRequestValidator.validateHeader(globalRequestHeader);
    }

    /**
     * X-Broker-API-Originating-Identity wrong base64-code for value ( given -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateHeaderOriginatingIdentityWrongBase64Value() {
        String xOriginatingIdentity = "cloudfoundry " + Base64.getEncoder().encodeToString("{{\"user_id\": \"683ea748-3092-4ff4-b656-39cacc4d5360\"}".getBytes(StandardCharsets.UTF_8));
        GlobalRequestHeader globalRequestHeader = new GlobalRequestHeader("2.14", xOriginatingIdentity);
        restRequestValidator.validateHeader(globalRequestHeader);
    }

    /**
     * X-Broker-API-Originating-Identity wrong value-key (user_name) given -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateHeaderOriginatingIdentityMissingInformationForPlatformCloudfoundry() {
        String xOriginatingIdentity = "cloudfoundry " + Base64.getEncoder().encodeToString("{\"user_name\": \"683ea748-3092-4ff4-b656-39cacc4d5360\"}".getBytes(StandardCharsets.UTF_8));
        GlobalRequestHeader globalRequestHeader = new GlobalRequestHeader("2.14", xOriginatingIdentity);
        restRequestValidator.validateHeader(globalRequestHeader);
    }

    /**
     * X-Broker-API-Originating-Identity with missing value-keys (kubernetes) -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateHeaderOriginatingIdentityMissingInformationForPlatformKubernetes() {
        String xOriginatingIdentity = "kubernetes " + Base64.getEncoder().encodeToString("{\"user_id\": \"683ea748-3092-4ff4-b656-39cacc4d5360\"}".getBytes(StandardCharsets.UTF_8));
        GlobalRequestHeader globalRequestHeader = new GlobalRequestHeader("2.14", xOriginatingIdentity);
        restRequestValidator.validateHeader(globalRequestHeader);
    }

    /**
     * X-Broker-API-Originating-Identity is supported
     */
    @Test
    public void validateHeaderOriginatingIdentitySuccess() {
        String xOriginatingIdentity = "cloudfoundry " + Base64.getEncoder().encodeToString("{\"user_id\": \"683ea748-3092-4ff4-b656-39cacc4d5360\"}".getBytes(StandardCharsets.UTF_8));
        GlobalRequestHeader globalRequestHeader = new GlobalRequestHeader("2.14", xOriginatingIdentity);
        restRequestValidator.validateHeader(globalRequestHeader);
    }
}
