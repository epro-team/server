package com.lanteam.kubernetesapi.server.controller;

import com.lanteam.kubernetesapi.server.model.Instance;
import com.lanteam.kubernetesapi.server.model.Operation;
import com.lanteam.kubernetesapi.server.model.OperationState;
import com.lanteam.kubernetesapi.server.model.ServiceInstancePreviousValues;
import com.lanteam.kubernetesapi.server.model.request.ProvisionServiceInstanceRequestBody;
import com.lanteam.kubernetesapi.server.model.request.ServiceInstanceUpdateRequest;
import com.lanteam.kubernetesapi.server.model.response.LastOperation;
import com.lanteam.kubernetesapi.server.model.response.ServiceInstanceResource;
import com.lanteam.kubernetesapi.server.model.response.ServiceInstanceResponse;
import com.lanteam.kubernetesapi.server.service.InstanceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceInstancesControllerTest {

    @MockBean
    RestRequestValidator restRequestValidator;
    @MockBean
    InstanceService instanceService;
    @Autowired
    ServiceInstancesController serviceInstancesController;

    /**
     * Check if the correct answer is returned when creating a new service instance
     */
    @Test
    public void provisionServicesInstanceTestForAccepted() {
        String dashboardUrl = "DummyDashboardURL";
        String operation = Operation.PROVISION.name() + "_UUID-PLACEHOLDER";

        ResponseEntity<ServiceInstanceResponse> expectedResponse =
                new ResponseEntity<>(
                        new ServiceInstanceResponse(
                                dashboardUrl,
                                operation
                        ),
                        HttpStatus.ACCEPTED
                );

        ProvisionServiceInstanceRequestBody requestBody =
                new ProvisionServiceInstanceRequestBody(
                        "DummyServiceID",
                        "DummyPlanID",
                        "DummyOrganizationID",
                        "DummySpaceId"
                );

        doAnswer(invocationOnMock -> {
            ProvisionServiceInstanceRequestBody body = invocationOnMock.getArgument(1);
            return new Instance(
                    body.getService_id(),
                    body.getPlan_id(),
                    null,
                    body.getContext(),
                    body.getOrganization_guid(),
                    body.getSpace_guid(),
                    body.getParameters(),
                    new HashMap<>(),
                    dashboardUrl,
                    operation,
                    null,
                    null
            );
        }).when(instanceService).provisionInstance(anyString(), any(ProvisionServiceInstanceRequestBody.class));

        ResponseEntity<ServiceInstanceResponse> response =
                serviceInstancesController.provisionServicesInstance(
                        "2.14",
                        null,
                        "DummyInstanceID",
                        true,
                        requestBody
                );

        assertEquals(expectedResponse, response);
    }

    /**
     * Check if the correct answer is returned when the service instance was already created
     */
    @Test
    public void provisionServicesInstanceTestForOk() {
        String dashboardUrl = "DummyDashboardURL";
        String operation = null;

        ResponseEntity<ServiceInstanceResponse> expectedResponse =
                new ResponseEntity<>(
                        new ServiceInstanceResponse(
                                dashboardUrl,
                                operation
                        ),
                        HttpStatus.OK
                );

        ProvisionServiceInstanceRequestBody requestBody =
                new ProvisionServiceInstanceRequestBody(
                        "DummyServiceID",
                        "DummyPlanID",
                        "DummyOrganizationID",
                        "DummySpaceId"
                );

        doAnswer(invocationOnMock -> {
            ProvisionServiceInstanceRequestBody body = invocationOnMock.getArgument(1);
            return new Instance(
                    body.getService_id(),
                    body.getPlan_id(),
                    null,
                    body.getContext(),
                    body.getOrganization_guid(),
                    body.getSpace_guid(),
                    body.getParameters(),
                    new HashMap<>(),
                    dashboardUrl,
                    operation,
                    null,
                    null
            );
        }).when(instanceService).provisionInstance(anyString(), any(ProvisionServiceInstanceRequestBody.class));

        ResponseEntity<ServiceInstanceResponse> response =
                serviceInstancesController.provisionServicesInstance(
                        "2.14",
                        null,
                        "DummyInstanceID",
                        true,
                        requestBody
                );

        assertEquals(expectedResponse, response);
    }

    /**
     * Check if the correct answer is returned when a new update for the service instance is initiated
     */
    @Test
    public void updateServicesInstanceTestForAccepted() {
        String dashboardUrl = "DummyDashboardUrl";
        String operation = Operation.UPDATE + "_UUID-PLACEHOLDER";

        ResponseEntity<ServiceInstanceResponse> expectedResponse =
                new ResponseEntity<>(
                        new ServiceInstanceResponse(
                                dashboardUrl,
                                operation
                        ),
                        HttpStatus.ACCEPTED
                );

        ServiceInstanceUpdateRequest request =
                new ServiceInstanceUpdateRequest(
                        null,
                        "DummyServiceID",
                        "NewPlanID",
                        null,
                        new ServiceInstancePreviousValues()
                );

        doAnswer(invocationOnMock -> {
            ServiceInstanceUpdateRequest body = invocationOnMock.getArgument(1);
            return new Instance(
                    body.getService_id(),
                    body.getPlan_id(),
                    null,
                    body.getContext(),
                    body.getPrevious_values().getOrganization_id(),
                    body.getPrevious_values().getSpace_id(),
                    body.getParameters(),
                    new HashMap<>(),
                    dashboardUrl,
                    null,
                    operation,
                    null
            );
        }).when(instanceService).updateInstance(anyString(), any(ServiceInstanceUpdateRequest.class));

        ResponseEntity<ServiceInstanceResponse> response =
                serviceInstancesController.updateServicesInstance(
                        "2.14",
                        null,
                        "DummyInstanceID",
                        true,
                        request
                );

        assertEquals(expectedResponse, response);
    }

    /**
     * Check if the correct answer is returned when a new update for the service instance is initiated
     */
    @Test
    public void updateServicesInstanceTestForOk() {
        String dashboardUrl = "DummyDashboardUrl";
        String operation = Operation.UPDATE + "_UUID-PLACEHOLDER";

        ResponseEntity<ServiceInstanceResponse> expectedResponse =
                new ResponseEntity<>(
                        new ServiceInstanceResponse(),
                        HttpStatus.OK
                );

        ServiceInstanceUpdateRequest request =
                new ServiceInstanceUpdateRequest(
                        null,
                        "DummyServiceID",
                        "NewPlanID",
                        null,
                        new ServiceInstancePreviousValues()
                );


        when(instanceService.getInstance(anyString())).thenReturn(
                new Instance(
                        "DummyServiceID",
                        "DummyPlanID",
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        dashboardUrl,
                        null,
                        null,
                        null
                )
        );

        when(restRequestValidator.validateInstanceUpdateConflictFree(anyString(), any(ServiceInstanceUpdateRequest.class))).thenReturn(true);

        ResponseEntity<ServiceInstanceResponse> response =
                serviceInstancesController.updateServicesInstance(
                        "2.14",
                        null,
                        "DummyInstanceID",
                        true,
                        request
                );

        assertEquals(expectedResponse, response);
    }

    /**
     * Check if the correct answer is returned when a the service instance should be deleted
     */
    @Test
    public void deleteServicesInstanceTestForAccepted() {
        String operation = Operation.DEPROVISION + "_UUID-PLACEHOLDER";

        ResponseEntity<ServiceInstanceResponse> expectedResponse =
                new ResponseEntity<>(
                        new ServiceInstanceResponse(
                                null,
                                operation
                        ),
                        HttpStatus.ACCEPTED
                );


        when(instanceService.deprovisionInstance(anyString())).thenReturn(
                new Instance(
                        "DummyServiceID",
                        "DummyPlanID",
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        operation
                )
        );

        ResponseEntity<ServiceInstanceResponse> response =
                serviceInstancesController.deleteServicesInstance(
                        "2.14",
                        "DummyInstanceID",
                        "DummyServiceID",
                        "DummyPlanID",
                        true
                );

        assertEquals(expectedResponse, response);
    }

    /**
     * Check if the correct answer is returned when the service instance should be returned
     */
    @Test
    public void getServicesInstanceTestForOk() {
        String service_id = "DummyServiceID";
        String plan_id = "DummyPlanID";
        String dashboard_url = "https://www.foo.bar/dashboard.php";
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("SomeKey", "SomeValue");
        parameters.put("CrazyParameter", "CrazyValue");

        ResponseEntity<ServiceInstanceResource> expectedResponse =
                new ResponseEntity<>(
                        new ServiceInstanceResource(
                                service_id,
                                plan_id,
                                dashboard_url,
                                parameters
                        ),
                        HttpStatus.OK
                );


        when(instanceService.getInstanceResource(anyString())).thenReturn(
                new ServiceInstanceResource(
                        service_id,
                        plan_id,
                        dashboard_url,
                        parameters
                )
        );

        ResponseEntity<ServiceInstanceResource> response =
                serviceInstancesController.getServicesInstance(
                        "2.14",
                        null,
                        service_id
                );

        assertEquals(expectedResponse, response);
    }

    /**
     * Check if the correct answer is returned when the last operation of a service instance is polled
     */
    @Test
    public void getLastOperationOfServicesInstanceTestForOkWithAnOperationRunning() {
        String description = "SomeDummyDescription";

        ResponseEntity<LastOperation> expectedResponse =
                new ResponseEntity<>(
                        new LastOperation(
                                OperationState.IN_PROGRESS,
                                description
                        ),
                        HttpStatus.OK
                );


        when(instanceService.lastOperationInstance(anyString(), isNull())).thenReturn(
                new LastOperation(
                        OperationState.IN_PROGRESS,
                        description
                )
        );

        ResponseEntity<LastOperation> response =
                serviceInstancesController.getLastOperationOfServicesInstance(
                        "2.14",
                        "SomeInstanceID",
                        "SomeServiceID",
                        "SomePlanID",
                        null
                );

        assertEquals(expectedResponse, response);
    }

    /**
     * Check if the correct answer is returned when the last operation of a service instance is polled
     */
    @Test
    public void getLastOperationOfServicesInstanceTestForOkWithNoOperationRunning() {
        String description = "SomeDummyDescription";

        ResponseEntity<LastOperation> expectedResponse =
                new ResponseEntity<>(
                        new LastOperation(),
                        HttpStatus.OK
                );


        when(instanceService.lastOperationInstance(anyString(), isNull())).thenReturn(null);

        ResponseEntity<LastOperation> response =
                serviceInstancesController.getLastOperationOfServicesInstance(
                        "2.14",
                        "SomeInstanceID",
                        "SomeServiceID",
                        "SomePlanID",
                        null
                );

        assertEquals(expectedResponse, response);
    }
}
