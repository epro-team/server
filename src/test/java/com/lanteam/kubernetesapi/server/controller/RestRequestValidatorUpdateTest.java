package com.lanteam.kubernetesapi.server.controller;

import com.lanteam.kubernetesapi.server.exceptions.BadRequestException;
import com.lanteam.kubernetesapi.server.exceptions.ElementGoneException;
import com.lanteam.kubernetesapi.server.exceptions.UnprocessableEntityException;
import com.lanteam.kubernetesapi.server.model.Instance;
import com.lanteam.kubernetesapi.server.model.Operation;
import com.lanteam.kubernetesapi.server.model.Service;
import com.lanteam.kubernetesapi.server.model.ServiceInstancePreviousValues;
import com.lanteam.kubernetesapi.server.model.request.ServiceBindingRequest;
import com.lanteam.kubernetesapi.server.model.request.ServiceInstanceUpdateRequest;
import com.lanteam.kubernetesapi.server.service.CatalogService;
import com.lanteam.kubernetesapi.server.service.HelmService;
import com.lanteam.kubernetesapi.server.util.helper.BindingFileManager;
import com.lanteam.kubernetesapi.server.util.helper.InstanceFileManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestRequestValidatorUpdateTest {

    @MockBean
    private InstanceFileManager instanceFileManagerMock;
    @MockBean
    private BindingFileManager bindingFileManagerMock;
    @MockBean
    private HelmService helmServiceMock;
    @Autowired
    private CatalogService catalogService;
    @Autowired
    private RestRequestValidator restRequestValidator;
    private String testService_id;
    private String testSmallPlan_id;
    private String testStandardPlan_id;
    private String testClusterPlan_id;
    private final String testStillProvisionedInstance_id = "TestInstanceStillProvisioning";
    private final String testStillUpdatedInstance_id = "TestInstanceStillUpdating";
    private final String testStillDeletedInstance_id = "TestInstanceStillDeleting";
    private final String testUsedSmallInstance_id = "TestSmallInstanceID";
    private final String testUsedStandardInstance_id = "TestStandardInstanceID";
    private final String testUsedClusterInstance_id = "TestClusterInstanceID";
    private final String testUsedBinding_id = "TestClusterBinding";
    private final String testOrganization_guid = "SomeIrrelevantOrganizationGUID";
    private final String testSpace_guid = "SomeIrrelevantSpaceGUID";

    /**
     * Get the id of the first service in the catalog and the corresponding small and cluster plan ids
     */
    @Before
    public void prepareTest() {
        Service service = catalogService.getServices().get(0);
        testService_id = service.getId();
        testSmallPlan_id = service.getPlans().stream()
                .filter(p -> p.getName().equals("small"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Couldnt find a small plan"))
                .getId();
        testStandardPlan_id = service.getPlans().stream()
                .filter(p -> p.getName().equals("standard"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Couldnt find a standard plan"))
                .getId();
        testClusterPlan_id = service.getPlans().stream()
                .filter(p -> p.getName().equals("cluster"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Couldnt find a cluster plan"))
                .getId();
        when(instanceFileManagerMock.loadFile()).thenReturn(loadTestInstances());
        when(bindingFileManagerMock.loadFile()).thenReturn(loadTestBindings());
    }

    /**
     * The simulated platform does not support async requests -> throw UnprocessableEntityException
     */
    @Test(expected = UnprocessableEntityException.class)
    public void validateUpdateServiceInstanceRequestTestAsyncNotSupported() {
        restRequestValidator.validateUpdateServiceInstanceRequest(null, null, false);
    }

    /**
     * The instance_id is set to null -> throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestInstanceIdIsNull() {
        restRequestValidator.validateUpdateServiceInstanceRequest(null, null, true);
    }

    /**
     * The instance_id is empty -> throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestInstanceIdIsEmpty() {
        restRequestValidator.validateUpdateServiceInstanceRequest("", null, true);
    }

    /**
     * The instance_id does not exist -> throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestInstanceIdDoesNotExist() {
        restRequestValidator.validateUpdateServiceInstanceRequest("ThisIsANotExistingInstanceId", null, true);
    }

    /**
     * The service_id is set to null -> throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestServiceIdIsNull() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedClusterInstance_id, request, true);
    }

    /**
     * The service_id is empty -> throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestServiceIdIsEmpty() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id("");
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedClusterInstance_id, request, true);
    }

    /**
     * The service_id does not exist in the catalog -> throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestServiceIdDoesNotExistInCatalog() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id("ThisServiceDoesNotExistInCatalog");
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedClusterInstance_id, request, true);
    }

    /**
     * The service_id exists but no other arguments are set, no error should be thrown
     */
    @Test
    public void validateUpdateServiceInstanceRequestTestServiceIdExistsNothingElseSet() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedClusterInstance_id, request, true);
    }

    /**
     * The plan_id is empty -> throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestPlanIdIsEmpty() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id("ThisServiceDoesNotExistInCatalog");
        request.setPlan_id("");
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedClusterInstance_id, request, true);
    }

    /**
     * The plan_id does not exist in catalog -> throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestPlanIdDoesNotExist() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        request.setPlan_id("ThisPlanIdDoesNotExistInTheCatalog");
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedClusterInstance_id, request, true);
    }

    /**
     * The plan_id exists in catalog and is same as current, no error should be thrown
     */
    @Test
    public void validateUpdateServiceInstanceRequestTestPlanIdSameAsCurrent() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        request.setPlan_id(testClusterPlan_id);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedClusterInstance_id, request, true);
    }

    /**
     * The plan_id exists in catalog and wants to switch from cluster to some other -> throw new BadRequestException, can onyl switch between small and standard
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestPlanIdSwitchFromCluster() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        request.setPlan_id(testSmallPlan_id);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedClusterInstance_id, request, true);
    }

    /**
     * The plan_id exists in catalog and is is a valid switch from small to standard plan, no error should be thrown
     */
    @Test
    public void validateUpdateServiceInstanceRequestTestPlanIdSwitchFromStandard() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        request.setPlan_id(testSmallPlan_id);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The previous_values set but no attribute in it, thats completely okay, no error should be thrown
     */
    @Test
    public void validateUpdateServiceInstanceRequestTestPreviousValuesWithNoData() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The service_id in previous_values is empty -> throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestPreviousValuesServiceIdIsEmpty() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        previousValues.setService_id("");
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The service_id in previous_values is not the same as in the service instance -> throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestPreviousValuesServiceIdIsWrong() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        previousValues.setService_id("ThisIsACompletelyWrongServiceId");
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The service_id in previous_values is the same as in the service instance, no error should be thrown
     */
    @Test
    public void validateUpdateServiceInstanceRequestTestPreviousValuesServiceIdIsSame() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        previousValues.setService_id(testService_id);
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The plan_id in previous_values is empty -> throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestPreviousValuesPlanIdIsEmpty() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        previousValues.setPlan_id("");
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The plan_id in previous_values is not the same as in the service instance -> throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestPreviousValuesPlanIdIsWrong() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        previousValues.setPlan_id("ThisIsACompletelyWrongPlanId");
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The plan_id in previous_values is the same as in the service instance, no error should be thrown
     */
    @Test
    public void validateUpdateServiceInstanceRequestTestPreviousValuesPlanIdIsSame() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        previousValues.setPlan_id(testStandardPlan_id);
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The organization_id in previous_values is empty -> throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestPreviousValuesOrganizationIdIsEmpty() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        previousValues.setOrganization_id("");
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The organization_id in previous_values is not the same as in the service instance -> throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestPreviousValuesOrganizationIdIsWrong() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        previousValues.setOrganization_id("ThisIsACompletelyWrongOrganizationId");
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The organization_id in previous_values is the same as in the service instance, no error should be thrown
     */
    @Test
    public void validateUpdateServiceInstanceRequestTestPreviousValuesOrganizationIdIsSame() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        previousValues.setOrganization_id(testOrganization_guid);
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The space_id in previous_values is empty -> throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestPreviousValuesSpaceIdIsEmpty() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        previousValues.setSpace_id("");
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The place_id in previous_values is not the same as in the service instance -> throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateUpdateServiceInstanceRequestTestPreviousValuesSpaceIdIsWrong() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        previousValues.setSpace_id("ThisIsACompletelyWrongSpaceId");
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * The place_id in previous_values is the same as in the service instance, no error should be thrown
     */
    @Test
    public void validateUpdateServiceInstanceRequestTestPreviousValuesSpaceIdIsSame() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        ServiceInstancePreviousValues previousValues = new ServiceInstancePreviousValues();
        previousValues.setSpace_id(testSpace_guid);
        request.setPrevious_values(previousValues);
        restRequestValidator.validateUpdateServiceInstanceRequest(testUsedStandardInstance_id, request, true);
    }

    /**
     * There is already some operation running on this instance, only one operation at the same time is allowed -> throw new UnprocessableEntityException
     */
    @Test(expected = UnprocessableEntityException.class)
    public void validateUpdateServiceInstanceRequestTestDifferentOperationAlreadyRunning() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        request.setPlan_id(testSmallPlan_id);
        restRequestValidator.validateUpdateServiceInstanceRequest(testStillDeletedInstance_id, request, true);
    }

    /**
     * There is already some update operation with different parameters running on this instance -> throw new UnprocessableEntityException
     */
    @Test(expected = UnprocessableEntityException.class)
    public void validateUpdateServiceInstanceRequestTestDifferentUpdateOperationAlreadyRunning() {
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        request.setPlan_id(testSmallPlan_id);
        restRequestValidator.validateUpdateServiceInstanceRequest(testStillUpdatedInstance_id, request, true);
    }

    /**
     * There is already some update operation with different parameters running on this instance -> throw new UnprocessableEntityException
     */
    @Test
    public void validateUpdateServiceInstanceRequestTestUpdateConflict() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("key1", "value1");
        parameters.put("key2", "value2");
        parameters.put("key3", "value3");
        ServiceInstanceUpdateRequest request = new ServiceInstanceUpdateRequest();
        request.setService_id(testService_id);
        request.setPlan_id(testSmallPlan_id);
        request.setParameters(parameters);
        assertFalse(restRequestValidator.validateInstanceUpdateConflictFree(testStillUpdatedInstance_id, request));
    }

    private HashMap<String, Instance> loadTestInstances() {
        HashMap<String, Instance> testData = new HashMap<>();
        Instance instance = new Instance(
                testService_id,
                testClusterPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                null,
                null
        );
        testData.put(testUsedClusterInstance_id, instance);
        instance = new Instance(
                testService_id,
                testStandardPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                null,
                null
        );
        testData.put(testUsedStandardInstance_id, instance);
        instance = new Instance(
                testService_id,
                testSmallPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                null,
                null
        );
        testData.put(testUsedSmallInstance_id, instance);
        instance = new Instance(
                testService_id,
                testSmallPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                Operation.PROVISION + "_UUID-PLACEHOLDER",
                null,
                null
        );
        testData.put(testStillProvisionedInstance_id, instance);
        instance = new Instance(
                testService_id,
                testStandardPlan_id,
                testSmallPlan_id,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                Operation.UPDATE + "_UUID-PLACEHOLDER",
                null
        );
        testData.put(testStillUpdatedInstance_id, instance);
        instance = new Instance(
                testService_id,
                testStandardPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                null,
                Operation.DEPROVISION + "_UUID-PLACEHOLDER"
        );
        testData.put(testStillDeletedInstance_id, instance);
        return testData;
    }

    private Map<String, Map<String, ServiceBindingRequest>> loadTestBindings() {
        ServiceBindingRequest serviceBindingRequest = new ServiceBindingRequest(
                null,
                testService_id,
                testClusterPlan_id,
                null,
                null,
                null
        );

        Map<String, ServiceBindingRequest> testBindings = new HashMap<>();
        testBindings.put(testUsedBinding_id, serviceBindingRequest);

        Map<String, Map<String, ServiceBindingRequest>> testData = new HashMap<>();
        testData.put(testUsedClusterInstance_id, testBindings);

        return testData;
    }
}
