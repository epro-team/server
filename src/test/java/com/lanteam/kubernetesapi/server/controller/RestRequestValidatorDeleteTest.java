package com.lanteam.kubernetesapi.server.controller;

import com.lanteam.kubernetesapi.server.exceptions.*;
import com.lanteam.kubernetesapi.server.model.Instance;
import com.lanteam.kubernetesapi.server.model.Operation;
import com.lanteam.kubernetesapi.server.model.Service;
import com.lanteam.kubernetesapi.server.model.ServiceInstancePreviousValues;
import com.lanteam.kubernetesapi.server.model.request.GlobalRequestHeader;
import com.lanteam.kubernetesapi.server.model.request.ProvisionServiceInstanceRequestBody;
import com.lanteam.kubernetesapi.server.model.request.ServiceBindingRequest;
import com.lanteam.kubernetesapi.server.model.request.ServiceInstanceUpdateRequest;
import com.lanteam.kubernetesapi.server.service.CatalogService;
import com.lanteam.kubernetesapi.server.service.HelmService;
import com.lanteam.kubernetesapi.server.util.helper.BindingFileManager;
import com.lanteam.kubernetesapi.server.util.helper.InstanceFileManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestRequestValidatorDeleteTest {

    @MockBean
    private InstanceFileManager instanceFileManagerMock;
    @MockBean
    private BindingFileManager bindingFileManagerMock;
    @MockBean
    private HelmService helmServiceMock;
    @Autowired
    private CatalogService catalogService;
    @Autowired
    private RestRequestValidator restRequestValidator;
    private String testService_id;
    private String testSmallPlan_id;
    private String testStandardPlan_id;
    private String testClusterPlan_id;
    private final String testStillProvisionedInstance_id = "TestInstanceStillProvisioning";
    private final String testStillUpdatedInstance_id = "TestInstanceStillUpdating";
    private final String testStillDeletedInstance_id = "TestInstanceStillDeleting";
    private final String testUsedSmallInstance_id = "TestSmallInstanceID";
    private final String testUsedStandardInstance_id = "TestStandardInstanceID";
    private final String testUsedClusterInstance_id = "TestClusterInstanceID";
    private final String testUsedBinding_id = "TestClusterBinding";
    private final String testOrganization_guid = "SomeIrrelevantOrganizationGUID";
    private final String testSpace_guid = "SomeIrrelevantSpaceGUID";

    /**
     * Get the id of the first service in the catalog and the corresponding small and cluster plan ids
     */
    @Before
    public void prepareTest() {
        Service service = catalogService.getServices().get(0);
        testService_id = service.getId();
        testSmallPlan_id = service.getPlans().stream()
                .filter(p -> p.getName().equals("small"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Couldnt find a small plan"))
                .getId();
        testStandardPlan_id = service.getPlans().stream()
                .filter(p -> p.getName().equals("standard"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Couldnt find a standard plan"))
                .getId();
        testClusterPlan_id = service.getPlans().stream()
                .filter(p -> p.getName().equals("cluster"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Couldnt find a cluster plan"))
                .getId();
        when(instanceFileManagerMock.loadFile()).thenReturn(loadTestInstances());
        when(bindingFileManagerMock.loadFile()).thenReturn(loadTestBindings());
    }

    /**
     * The simulated platform does not support async requests -> throw UnprocessableEntityException
     */
    @Test(expected = UnprocessableEntityException.class)
    public void validateDeleteServiceInstanceRequestTestAsyncNotSupported() {
        restRequestValidator.validateDeleteServiceInstanceRequest(null, null, null, false);
    }

    /**
     * The given instance_id is null -> has to throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateDeleteServiceInstanceRequestTestInstanceIdIsNull() {
        restRequestValidator.validateDeleteServiceInstanceRequest(null, null, null, true);
    }

    /**
     * The given instance_id is empty -> has to throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateDeleteServiceInstanceRequestTestInstanceIdIsEmpty() {
        restRequestValidator.validateDeleteServiceInstanceRequest("", null, null, true);
    }

    /**
     * The given instance_id is not existing -> has to throw new ElementGoneException
     */
    @Test(expected = ElementGoneException.class)
    public void validateDeleteServiceInstanceRequestTestInstanceIdIsNotExisting() {
        restRequestValidator.validateDeleteServiceInstanceRequest("ThisInstanceIdDoesNotExist", null, null, true);
    }

    /**
     * The given service_id is null -> has to throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateDeleteServiceInstanceRequestTestServiceIdIsNull() {
        restRequestValidator.validateDeleteServiceInstanceRequest(testUsedClusterInstance_id, null, null, true);
    }

    /**
     * The given service_id is emtpy -> has to throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateDeleteServiceInstanceRequestTestServiceIdIsEmpty() {
        restRequestValidator.validateDeleteServiceInstanceRequest(testUsedClusterInstance_id, "", null, true);
    }

    /**
     * The given service_id is not associated with the instance_id -> has to throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateDeleteServiceInstanceRequestTestServiceIdIsNotAssociatedWithInstanceId() {
        restRequestValidator.validateDeleteServiceInstanceRequest(testUsedClusterInstance_id, "NotAssociatedServiceId", null, true);
    }

    /**
     * The given plan_id is null -> has to throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateDeleteServiceInstanceRequestTestPlanIdIsNull() {
        restRequestValidator.validateDeleteServiceInstanceRequest(testUsedClusterInstance_id, testService_id, null, true);
    }

    /**
     * The given service_id is emtpy -> has to throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateDeleteServiceInstanceRequestTestPlanIdIsEmpty() {
        restRequestValidator.validateDeleteServiceInstanceRequest(testUsedClusterInstance_id, testService_id, "", true);
    }

    /**
     * The given plan_id is not associated with the instance_id -> has to throw new BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateDeleteServiceInstanceRequestTestPlanIdIsNotAssociatedWithInstanceId() {
        restRequestValidator.validateDeleteServiceInstanceRequest(testUsedClusterInstance_id, testService_id, "NotAssociatedPlanId", true);
    }

    /**
     * Another Operation is already running on this instance_id -> has to throw new
     */
    @Test(expected = UnprocessableEntityException.class)
    public void validateDeleteServiceInstanceRequestTestOtherOperationRunnig() {
        restRequestValidator.validateDeleteServiceInstanceRequest(testStillUpdatedInstance_id, testService_id, testStandardPlan_id, true);
    }

    /**
     * All parameters correctly set, has to succeed
     */
    @Test
    public void validateDeleteServiceInstanceRequestTestSuccess() {
        restRequestValidator.validateDeleteServiceInstanceRequest(testUsedClusterInstance_id, testService_id, testClusterPlan_id, true);
    }

    private HashMap<String, Instance> loadTestInstances() {
        HashMap<String, Instance> testData = new HashMap<>();
        Instance instance = new Instance(
                testService_id,
                testClusterPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                null,
                null
        );
        testData.put(testUsedClusterInstance_id, instance);
        instance = new Instance(
                testService_id,
                testStandardPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                null,
                null
        );
        testData.put(testUsedStandardInstance_id, instance);
        instance = new Instance(
                testService_id,
                testSmallPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                null,
                null
        );
        testData.put(testUsedSmallInstance_id, instance);
        instance = new Instance(
                testService_id,
                testSmallPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                Operation.PROVISION + "_UUID-PLACEHOLDER",
                null,
                null
        );
        testData.put(testStillProvisionedInstance_id, instance);
        instance = new Instance(
                testService_id,
                testStandardPlan_id,
                testSmallPlan_id,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                Operation.UPDATE + "_UUID-PLACEHOLDER",
                null
        );
        testData.put(testStillUpdatedInstance_id, instance);
        instance = new Instance(
                testService_id,
                testStandardPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                null,
                Operation.DEPROVISION + "_UUID-PLACEHOLDER"
        );
        testData.put(testStillDeletedInstance_id, instance);
        return testData;
    }

    private Map<String, Map<String, ServiceBindingRequest>> loadTestBindings() {
        ServiceBindingRequest serviceBindingRequest = new ServiceBindingRequest(
                null,
                testService_id,
                testClusterPlan_id,
                null,
                null,
                null
        );

        Map<String, ServiceBindingRequest> testBindings = new HashMap<>();
        testBindings.put(testUsedBinding_id, serviceBindingRequest);

        Map<String, Map<String, ServiceBindingRequest>> testData = new HashMap<>();
        testData.put(testUsedClusterInstance_id, testBindings);

        return testData;
    }
}
