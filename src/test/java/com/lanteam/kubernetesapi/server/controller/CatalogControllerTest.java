package com.lanteam.kubernetesapi.server.controller;

import com.lanteam.kubernetesapi.server.model.Service;
import com.lanteam.kubernetesapi.server.model.response.Catalog;
import com.lanteam.kubernetesapi.server.service.CatalogService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CatalogControllerTest {

    @MockBean
    CatalogService catalogService;
    @MockBean
    RestRequestValidator restRequestValidator;
    @Autowired
    CatalogController catalogController;

    @Test
    public void getCatalogTest(){
        Service service = new Service("DummyName", "DummyID", "DummyDescription", false, new ArrayList<>());

        ArrayList<Service> services = new ArrayList<>();
        services.add(service);
        when(catalogService.getServices()).thenReturn(services);

        Catalog expectedCatalog = new Catalog(services);

        Catalog catalog = catalogController.getCatalog("2.14");

        assertEquals(expectedCatalog, catalog);
    }
}
