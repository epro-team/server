package com.lanteam.kubernetesapi.server.controller;

import com.lanteam.kubernetesapi.server.exceptions.*;
import com.lanteam.kubernetesapi.server.model.Instance;
import com.lanteam.kubernetesapi.server.model.Operation;
import com.lanteam.kubernetesapi.server.model.Service;
import com.lanteam.kubernetesapi.server.model.ServiceInstancePreviousValues;
import com.lanteam.kubernetesapi.server.model.request.ProvisionServiceInstanceRequestBody;
import com.lanteam.kubernetesapi.server.model.request.ServiceBindingRequest;
import com.lanteam.kubernetesapi.server.model.request.ServiceInstanceUpdateRequest;
import com.lanteam.kubernetesapi.server.service.CatalogService;
import com.lanteam.kubernetesapi.server.service.HelmService;
import com.lanteam.kubernetesapi.server.util.helper.BindingFileManager;
import com.lanteam.kubernetesapi.server.util.helper.InstanceFileManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestRequestValidatorProvisionTest {

    @MockBean
    private InstanceFileManager instanceFileManagerMock;
    @MockBean
    private BindingFileManager bindingFileManagerMock;
    @MockBean
    private HelmService helmServiceMock;
    @Autowired
    private CatalogService catalogService;
    @Autowired
    private RestRequestValidator restRequestValidator;
    private String testService_id;
    private String testSmallPlan_id;
    private String testStandardPlan_id;
    private String testClusterPlan_id;
    private final String testStillProvisionedInstance_id = "TestInstanceStillProvisioning";
    private final String testStillUpdatedInstance_id = "TestInstanceStillUpdating";
    private final String testStillDeletedInstance_id = "TestInstanceStillDeleting";
    private final String testUsedSmallInstance_id = "TestSmallInstanceID";
    private final String testUsedStandardInstance_id = "TestStandardInstanceID";
    private final String testUsedClusterInstance_id = "TestClusterInstanceID";
    private final String testUsedBinding_id = "TestClusterBinding";
    private final String testOrganization_guid = "SomeIrrelevantOrganizationGUID";
    private final String testSpace_guid = "SomeIrrelevantSpaceGUID";

    /**
     * Get the id of the first service in the catalog and the corresponding small and cluster plan ids
     */
    @Before
    public void prepareTest() {
        Service service = catalogService.getServices().get(0);
        testService_id = service.getId();
        testSmallPlan_id = service.getPlans().stream()
                .filter(p -> p.getName().equals("small"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Couldnt find a small plan"))
                .getId();
        testStandardPlan_id = service.getPlans().stream()
                .filter(p -> p.getName().equals("standard"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Couldnt find a standard plan"))
                .getId();
        testClusterPlan_id = service.getPlans().stream()
                .filter(p -> p.getName().equals("cluster"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Couldnt find a cluster plan"))
                .getId();
        when(instanceFileManagerMock.loadFile()).thenReturn(loadTestInstances());
        when(bindingFileManagerMock.loadFile()).thenReturn(loadTestBindings());
    }

    /**
     * The given instance id is null -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateProvisionServiceInstanceRequestTestIdIsNull() {
        restRequestValidator.validateProvisionServiceInstanceRequest(null, null, false);
    }

    /**
     * The given instance id is empty -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateProvisionServiceInstanceRequestTestIdIsEmpty() {
        restRequestValidator.validateProvisionServiceInstanceRequest("", null, false);
    }

    /**
     * The given simulate a platform that does not support asynchronous requests -> has to throw UnprocessableEntityException
     */
    @Test(expected = UnprocessableEntityException.class)
    public void validateProvisionServiceInstanceRequestTestAsyncNotSupported() {
        restRequestValidator.validateProvisionServiceInstanceRequest("SomeRandomNotExistingID", null, false);
    }

    /**
     * The given service id is empty -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateProvisionServiceInstanceRequestTestServiceIdIsEmpty() {
        ProvisionServiceInstanceRequestBody body =
                new ProvisionServiceInstanceRequestBody(
                        "",
                        "",
                        null,
                        "",
                        "",
                        null
                );
        restRequestValidator.validateProvisionServiceInstanceRequest("SomeRandomNotExistingID", body, true);
    }

    /**
     * The given service id does not exist in the catalog-> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateProvisionServiceInstanceRequestTestServiceIdDoesNotExist() {
        ProvisionServiceInstanceRequestBody body =
                new ProvisionServiceInstanceRequestBody(
                        "SomeServiceIdThatDoesntExist",
                        "",
                        null,
                        "",
                        "",
                        null
                );
        restRequestValidator.validateProvisionServiceInstanceRequest("SomeRandomNotExistingID", body, true);
    }

    /**
     * The given plan id is empty -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateProvisionServiceInstanceRequestTestPlanIdIsEmpty() {
        ProvisionServiceInstanceRequestBody body =
                new ProvisionServiceInstanceRequestBody(
                        testService_id,
                        "",
                        null,
                        "",
                        "",
                        null
                );
        restRequestValidator.validateProvisionServiceInstanceRequest("SomeRandomNotExistingID", body, true);
    }

    /**
     * The given plan id does not exist in the catalog-> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateProvisionServiceInstanceRequestTestPlanIdDoesNotExist() {
        ProvisionServiceInstanceRequestBody body =
                new ProvisionServiceInstanceRequestBody(
                        testService_id,
                        "SomePlanIdThatDoesNotExist",
                        null,
                        "",
                        "",
                        null
                );
        restRequestValidator.validateProvisionServiceInstanceRequest("SomeRandomNotExistingID", body, true);
    }

    /**
     * The given organization_guid is not set -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateProvisionServiceInstanceRequestTestOrganizationGuidNotSet() {
        ProvisionServiceInstanceRequestBody body =
                new ProvisionServiceInstanceRequestBody(
                        testService_id,
                        testSmallPlan_id,
                        null,
                        "",
                        "",
                        null
                );
        restRequestValidator.validateProvisionServiceInstanceRequest("SomeRandomNotExistingID", body, true);
    }

    /**
     * The given space_guid is not set -> has to throw BadRequestException
     */
    @Test(expected = BadRequestException.class)
    public void validateProvisionServiceInstanceRequestTestSpaceGuidNotSet() {
        ProvisionServiceInstanceRequestBody body =
                new ProvisionServiceInstanceRequestBody(
                        testService_id,
                        testClusterPlan_id,
                        null,
                        testOrganization_guid,
                        "",
                        null
                );
        restRequestValidator.validateProvisionServiceInstanceRequest("SomeRandomNotExistingID", body, true);
    }

    /**
     * The given instance_id is already used with different Arguments-> has to throw ConflictException
     */
    @Test(expected = ConflictException.class)
    public void validateProvisionServiceInstanceRequestTestInstanceIdUsedWithDifferentArguments() {
        HashMap<String, String> differentParameters = new HashMap<>();
        differentParameters.put("New", "different");
        ProvisionServiceInstanceRequestBody body =
                new ProvisionServiceInstanceRequestBody(
                        testService_id,
                        testClusterPlan_id,
                        null,
                        testOrganization_guid,
                        testSpace_guid,
                        differentParameters
                );
        restRequestValidator.validateProvisionServiceInstanceRequest(testUsedClusterInstance_id, body, true);
    }

    /**
     * The given instance_id is already used with same Arguments
     */
    @Test
    public void validateProvisionServiceInstanceRequestTestInstanceIdUsedWithSameArguments() {
        ProvisionServiceInstanceRequestBody body =
                new ProvisionServiceInstanceRequestBody(
                        testService_id,
                        testClusterPlan_id,
                        null,
                        testOrganization_guid,
                        testSpace_guid,
                        null
                );
        restRequestValidator.validateProvisionServiceInstanceRequest(testUsedClusterInstance_id, body, true);
    }

    private HashMap<String, Instance> loadTestInstances() {
        HashMap<String, Instance> testData = new HashMap<>();
        Instance instance = new Instance(
                testService_id,
                testClusterPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                null,
                null
        );
        testData.put(testUsedClusterInstance_id, instance);
        instance = new Instance(
                testService_id,
                testStandardPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                null,
                null
        );
        testData.put(testUsedStandardInstance_id, instance);
        instance = new Instance(
                testService_id,
                testSmallPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                null,
                null
        );
        testData.put(testUsedSmallInstance_id, instance);
        instance = new Instance(
                testService_id,
                testSmallPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                Operation.PROVISION + "_UUID-PLACEHOLDER",
                null,
                null
        );
        testData.put(testStillProvisionedInstance_id, instance);
        instance = new Instance(
                testService_id,
                testStandardPlan_id,
                testSmallPlan_id,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                Operation.UPDATE + "_UUID-PLACEHOLDER",
                null
        );
        testData.put(testStillUpdatedInstance_id, instance);
        instance = new Instance(
                testService_id,
                testStandardPlan_id,
                null,
                null,
                testOrganization_guid,
                testSpace_guid,
                null,
                new HashMap<>(),
                "http://www.foo.bar/dashboard.php",
                null,
                null,
                Operation.DEPROVISION + "_UUID-PLACEHOLDER"
        );
        testData.put(testStillDeletedInstance_id, instance);
        return testData;
    }

    private Map<String, Map<String, ServiceBindingRequest>> loadTestBindings() {
        ServiceBindingRequest serviceBindingRequest = new ServiceBindingRequest(
                null,
                testService_id,
                testClusterPlan_id,
                null,
                null,
                null
        );

        Map<String, ServiceBindingRequest> testBindings = new HashMap<>();
        testBindings.put(testUsedBinding_id, serviceBindingRequest);

        Map<String, Map<String, ServiceBindingRequest>> testData = new HashMap<>();
        testData.put(testUsedClusterInstance_id, testBindings);

        return testData;
    }
}
