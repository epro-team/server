package com.lanteam.kubernetesapi.server.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        RestRequestValidatorHeaderTest.class,
        RestRequestValidatorLastOperationTest.class,
        RestRequestValidatorProvisionTest.class,
        RestRequestValidatorGetTest.class,
        RestRequestValidatorUpdateTest.class,
        RestRequestValidatorDeleteTest.class,
        RestRequestValidatorGenerateBindingTest.class,
        RestRequestValidatorDeleteBindingTest.class
})
public class RestRequestValidatorSuite {
}
