package com.lanteam.kubernetesapi.server.controller;

import com.lanteam.kubernetesapi.server.exceptions.BadRequestException;
import com.lanteam.kubernetesapi.server.model.Credentials;
import com.lanteam.kubernetesapi.server.model.EmptyResponse;
import com.lanteam.kubernetesapi.server.model.Instance;
import com.lanteam.kubernetesapi.server.model.Operation;
import com.lanteam.kubernetesapi.server.model.request.ProvisionServiceInstanceRequestBody;
import com.lanteam.kubernetesapi.server.model.request.ServiceBindingRequest;
import com.lanteam.kubernetesapi.server.model.response.ServiceBinding;
import com.lanteam.kubernetesapi.server.model.response.ServiceBindingResource;
import com.lanteam.kubernetesapi.server.model.response.ServiceInstanceResponse;
import com.lanteam.kubernetesapi.server.service.BindingService;
import com.lanteam.kubernetesapi.server.service.InstanceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceBindingsControllerTest {

    @MockBean
    RestRequestValidator restRequestValidator;
    @MockBean
    InstanceService instanceService;
    @MockBean
    BindingService bindingService;
    @Autowired
    ServiceBindingsController serviceBindingsController;

    /**
     * Check if the correct answer is returned when creating a new service binding
     */
    @Test
    public void generateServiceBindingTestForAccepted() {
        String hostname = "1.0.0.127";
        int port = 666;
        String password = "P4SSW0RD!";

        ResponseEntity<ServiceBinding> expectedResponse =
                new ResponseEntity<>(
                        new ServiceBinding(
                                new Credentials(
                                        hostname,
                                        port,
                                        password
                                ),
                                null,
                                null,
                                null
                        ),
                        HttpStatus.CREATED
                );

        ServiceBindingRequest requestBody =
                new ServiceBindingRequest(
                        null,
                        "DummyServiceID",
                        "DummyPlanID",
                        "SomeAppGuid",
                        null,
                        null
                );

        doAnswer(invocationOnMock -> {
            ServiceBindingRequest body = invocationOnMock.getArgument(2);
            return new ServiceBinding(
                    new Credentials(
                            hostname,
                            port,
                            password
                    ),
                    null,
                    null,
                    null
            );
        }).when(bindingService).createBinding(anyString(), anyString(), any(ServiceBindingRequest.class));

        ResponseEntity<ServiceBinding> response =
                serviceBindingsController.generateServiceBinding(
                        "2.14",
                        null,
                        "DummyInstanceID",
                        "DummyBindingID",
                        true,
                        requestBody
                );

        assertEquals(expectedResponse, response);
    }

    /**
     * Check if the correct answer is returned when the service binding already exists
     */
    @Test
    public void generateServiceBindingTestForOk() {
        String hostname = "1.0.0.127";
        int port = 666;
        String password = "P4SSW0RD!";
        Credentials credentials = new Credentials(hostname, port, password);

        ResponseEntity<ServiceBinding> expectedResponse =
                new ResponseEntity<>(
                        new ServiceBinding(
                                credentials,
                                null,
                                null,
                                null
                        ),
                        HttpStatus.OK
                );

        ServiceBindingRequest requestBody =
                new ServiceBindingRequest(
                        null,
                        "DummyServiceID",
                        "DummyPlanID",
                        "SomeAppGuid",
                        null,
                        null
                );

        when(bindingService.getBinding(anyString(), anyString())).thenReturn(mock(ServiceBindingRequest.class));
        when(bindingService.getCredentials(anyString())).thenReturn(credentials);

        ResponseEntity<ServiceBinding> response =
                serviceBindingsController.generateServiceBinding(
                        "2.14",
                        null,
                        "DummyInstanceID",
                        "DummyBindingID",
                        true,
                        requestBody
                );

        assertEquals(expectedResponse, response);
    }

    /**
     * Check if the correct answer is returned when the service binding is deleted
     */
    @Test
    public void deleteServiceBindingTestForOk() {

        ResponseEntity<EmptyResponse> expectedResponse =
                new ResponseEntity<>(
                        new EmptyResponse(),
                        HttpStatus.OK
                );

        ResponseEntity<EmptyResponse> response =
                serviceBindingsController.deleteServiceBinding(
                        "2.14",
                        null,
                        "DummyInstanceID",
                        "DummyBindingID",
                        "DummyServiceID",
                        "DummyPlanID",
                        false
                );

        assertEquals(expectedResponse, response);
    }

    /**
     * Check if the correct answer is returned when the last operation of the service binding is polled
     */
    @Test(expected = BadRequestException.class)
    public void getLastOperationOfServiceBindingTest() {
        serviceBindingsController.getLastOperationOfServiceBinding(
                "2.14",
                "DummyInstanceID",
                "DummyBindingID",
                "DummyServiceID",
                "DummyPlanID",
                null
        );
    }

    /**
     * Check if the correct answer is returned when the service binding is fetched
     */
    @Test
    public void getServiceBindingTestForOk() {
        String hostname = "1.0.0.127";
        int port = 666;
        String password = "P4SSW0RD!";
        Credentials credentials = new Credentials(hostname, port, password);
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("param1", "value1");
        parameters.put("param2", "value2");
        parameters.put("param3", "value3");

        ResponseEntity<ServiceBindingResource> expectedResponse =
                new ResponseEntity<>(
                        new ServiceBindingResource(
                                credentials,
                                null,
                                null,
                                null,
                                parameters
                        ),
                        HttpStatus.OK
                );

        when(bindingService.getBinding(anyString(), anyString())).thenReturn(
                new ServiceBindingRequest(
                        null,
                        "DummyServiceID",
                        "DummyPlanID",
                        "DummyAppGuid",
                        null,
                        parameters
                )
        );
        when(bindingService.getCredentials(any())).thenReturn(credentials);

        ResponseEntity<ServiceBindingResource> response =
                serviceBindingsController.getServiceBinding(
                        "2.14",
                        null,
                        "DummyInstanceID",
                        "DummyBindingID"
                );

        assertEquals(expectedResponse, response);
    }
}
