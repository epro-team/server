package com.lanteam.kubernetesapi.server.exceptions;

import com.lanteam.kubernetesapi.server.model.ErrorCode;
import org.springframework.http.HttpStatus;

public class ElementGoneException extends OpenServiceBrokerException{

    public ElementGoneException(ErrorCode errorCode, String message) {
        super(errorCode, message, HttpStatus.GONE);
    }
}
