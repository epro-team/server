package com.lanteam.kubernetesapi.server.exceptions;

import com.lanteam.kubernetesapi.server.model.ErrorCode;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public abstract class OpenServiceBrokerException extends RuntimeException {

    private ErrorCode errorCode;
    private HttpStatus httpStatus;

    protected OpenServiceBrokerException(ErrorCode errorCode, String message, HttpStatus httpStatus){
        super(message);
        this.errorCode = errorCode;
        this.httpStatus = httpStatus;
    }

}
