package com.lanteam.kubernetesapi.server.exceptions;

import com.lanteam.kubernetesapi.server.model.ErrorCode;
import org.springframework.http.HttpStatus;

public class UnsupportedApiLevelException extends OpenServiceBrokerException {

    public UnsupportedApiLevelException(ErrorCode errorCode, String message) {
        super(errorCode, message, HttpStatus.PRECONDITION_FAILED);
    }
}
