package com.lanteam.kubernetesapi.server.exceptions;

import com.lanteam.kubernetesapi.server.model.ErrorCode;
import org.springframework.http.HttpStatus;

public class InternalServerErrorException extends OpenServiceBrokerException {

    public InternalServerErrorException(ErrorCode errorCode, String message) {
        super(errorCode, message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
