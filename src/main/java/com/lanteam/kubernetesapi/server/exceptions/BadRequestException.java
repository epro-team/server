package com.lanteam.kubernetesapi.server.exceptions;

import com.lanteam.kubernetesapi.server.model.ErrorCode;
import org.springframework.http.HttpStatus;

public class BadRequestException extends OpenServiceBrokerException {

    public BadRequestException(ErrorCode errorCode, String message) {
        super(errorCode, message, HttpStatus.BAD_REQUEST);
    }
}
