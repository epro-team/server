package com.lanteam.kubernetesapi.server.util.helper;

import static com.lanteam.kubernetesapi.server.util.helper.HelmHelper.KEYS.SLUG;

/**
 * Helm helper
 */
public class HelmHelper {
	/**
	 * Constance
	 */
	public static class KEYS {
		/**
		 * Slug which is used to hide a possible int from an uuid id because k8 doesn't support ids with starting int
		 * (DNS / IP stuff)
		 */
		public static final String SLUG       = "lanteam-";
		/**
		 * the helm Package name, path and version
		 */
		public static final String REDIS_HELM = "./redis-1.2.1.tgz";
	}

	/**
	 * prepend slug to id
	 *
	 * @param instanceID instance id
	 *
	 * @return instance id with slug
	 */
	public static String makeSlug(String instanceID) {
		return !instanceID.startsWith(SLUG) ? SLUG + instanceID : instanceID;
	}

	/**
	 * remove the slug form an instance id
	 *
	 * @param instanceId instance id
	 *
	 * @return slug free instance id
	 */
	public static String removeSlug(final String instanceId) {
		return instanceId.replaceFirst(SLUG, "");
	}
}
