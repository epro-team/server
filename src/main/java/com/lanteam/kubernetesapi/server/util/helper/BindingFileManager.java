package com.lanteam.kubernetesapi.server.util.helper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lanteam.kubernetesapi.server.model.request.ServiceBindingRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * The wunderfull and magic binding file manager
 * <p>
 * handles all necessary stuff for a simple write and read to disk
 */
@Slf4j
@Component
public class BindingFileManager {
	/**
	 * The magical object mapper
	 */
	private final ObjectMapper objectMapper;
	/**
	 * file path
	 */
	private final File         fileAccess;

	/**
	 * DI Constructor
	 *
	 * @param objectMapper objectMapper
	 */
	@Autowired
	public BindingFileManager(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
		this.fileAccess = new File("bindings.json");
	}

	/**
	 * add a binding
	 *
	 * @param instance_id           instance id
	 * @param binding_id            binding id
	 * @param serviceBindingRequest request
	 */
	public synchronized void addBinding(String instance_id, String binding_id, ServiceBindingRequest serviceBindingRequest) {
		Map<String, Map<String, ServiceBindingRequest>> instanceBindings = loadFile();
		Map<String, ServiceBindingRequest>              bindings         = instanceBindings.get(instance_id);
		if (bindings == null) {
			bindings = new HashMap<>();
		}
		bindings.put(binding_id, serviceBindingRequest);
		instanceBindings.put(instance_id, bindings);
		writeToFile(instanceBindings);
	}

	/**
	 * remove a binding
	 *
	 * @param instance_id instance id
	 * @param binding_id  binding id
	 */
	public synchronized void removeBinding(String instance_id, String binding_id) {
		Map<String, Map<String, ServiceBindingRequest>> instanceBindings = loadFile();
		Map<String, ServiceBindingRequest>              bindings         = instanceBindings.get(instance_id);
		bindings.remove(binding_id);
		if (bindings.size() == 0) {
			instanceBindings.remove(instance_id);
		} else {
			instanceBindings.put(instance_id, bindings);
		}
		writeToFile(instanceBindings);
	}

	/**
	 * lead a file from disk in json format
	 *
	 * @return data as map
	 */
	public synchronized Map<String, Map<String, ServiceBindingRequest>> loadFile() {
		Map<String, Map<String, ServiceBindingRequest>> map;
		if (fileAccess.exists()) {
			try {
				map = objectMapper.readValue(
						fileAccess,
						new TypeReference<HashMap<String, HashMap<String, ServiceBindingRequest>>>() {
						}
				);
			} catch (IOException e) {
				map = new HashMap<>();
			}
		} else {
			map = new HashMap<>();
		}
		return map;
	}

	/**
	 * write a file to disk
	 *
	 * @param map data which will be translated to json
	 */
	private synchronized void writeToFile(Map<String, Map<String, ServiceBindingRequest>> map) {
		try {
			objectMapper.writeValue(fileAccess, map);
		} catch (IOException e) {
			log.error("File writeing failed:", e);
		}
	}
}
