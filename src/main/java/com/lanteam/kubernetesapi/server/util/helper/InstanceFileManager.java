package com.lanteam.kubernetesapi.server.util.helper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lanteam.kubernetesapi.server.model.Instance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * The wunderfull and magic file manager
 * <p>
 * handles all necessary stuff for a simple write and read to disk
 */
@Slf4j
@Component
public class InstanceFileManager {
	/**
	 * The magical object mapper
	 */
	private final ObjectMapper objectMapper;
	/**
	 * file path
	 */
	private final File         fileAccess;

	/**
	 * DI Constructor
	 *
	 * @param objectMapper objectMapper
	 */
	@Autowired
	public InstanceFileManager(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
		this.fileAccess = new File("instances.json");
	}

	/**
	 * add a new instance
	 *
	 * @param instance_id instance id
	 * @param instance    instance
	 */
	public synchronized void putInstance(String instance_id, Instance instance) {
		HashMap<String, Instance> instances = loadFile();
		instances.put(instance_id, instance);
		writeToFile(instances);
	}

	/**
	 * remove an instance
	 *
	 * @param instance_id instance id
	 */
	public synchronized void removeInstance(String instance_id) {
		HashMap<String, Instance> instances = loadFile();
		instances.remove(instance_id);
		writeToFile(instances);
	}

	/**
	 * lead a file from disk in json format
	 *
	 * @return data as map
	 */
	public synchronized HashMap<String, Instance> loadFile() {
		HashMap<String, Instance> map = null;
		if (fileAccess.exists()) {
			try {
				map = objectMapper.readValue(fileAccess, new TypeReference<HashMap<String, Instance>>() {
				});
			} catch (IOException e) {
				map = new HashMap<>();
			}
		} else {
			map = new HashMap<>();
		}
		return map;
	}

	/**
	 * write a file to disk
	 *
	 * @param map data which will be translated to json
	 */
	private synchronized void writeToFile(HashMap<String, Instance> map) {
		try {
			objectMapper.writeValue(fileAccess, map);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
