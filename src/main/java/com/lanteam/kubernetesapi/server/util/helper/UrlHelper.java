package com.lanteam.kubernetesapi.server.util.helper;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

/**
 * simple static url helper methods
 */
public class UrlHelper {

	/**
	 * load a chart by http url
	 *
	 * @param httpsUrl http url
	 *
	 * @return chart
	 *
	 * @throws MalformedURLException url is invalid
	 */
	public static URL getHttpChart(String httpsUrl) throws MalformedURLException {
		return URI.create(httpsUrl).toURL();
	}

	/**
	 * loads a local chart
	 *
	 * @param localFile local file
	 *
	 * @return chart
	 *
	 * @throws MalformedURLException url is invalid
	 */
	public static URL getLocalChart(File localFile) throws MalformedURLException {
		return localFile.toURI().toURL();
	}
}
