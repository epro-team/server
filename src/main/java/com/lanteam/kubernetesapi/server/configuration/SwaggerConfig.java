package com.lanteam.kubernetesapi.server.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Swagger Config
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport {
	/**
	 * get swagger config and data
	 *
	 * @return Docket object
	 */
	@Bean
	public Docket apiDocket() {
		List<SecurityScheme> securityList = new ArrayList<>();
		securityList.add(new BasicAuth("basicAuth"));

		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.lanteam.kubernetesapi.server.controller"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(
						new ApiInfo(
								"Open Service Broker API",
								"The Open Service Broker API defines an HTTP(S) interface between Platforms and Service Brokers.",
								"v2.14",
								"/usage",
								new Contact("LANteam", "/contact", "mail@lan.team"),
								"LANteam common license",
								"/license",
								Collections.emptyList()
						)
				)
				.useDefaultResponseMessages(false)
				.securitySchemes(securityList);
	}

	/**
	 * Override this method to add resource handlers for serving static resources.
	 *
	 * @see ResourceHandlerRegistry
	 */
	@Override
	protected void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("swagger-ui.html")
		        .addResourceLocations("classpath:/META-INF/resources/");

		registry.addResourceHandler("/webjars/**")
		        .addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
}