package com.lanteam.kubernetesapi.server.configuration;

import com.lanteam.kubernetesapi.server.util.security.CustomBasicAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Spring Security
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	/**
	 * CustomBasicAuthenticationEntryPoint
	 */
	private final CustomBasicAuthenticationEntryPoint authenticationEntryPoint;

	/**
	 * DI Constructor
	 *
	 * @param authenticationEntryPoint CustomBasicAuthenticationEntryPoint
	 */
	@Autowired
	public SecurityConfig(final CustomBasicAuthenticationEntryPoint authenticationEntryPoint) {this.authenticationEntryPoint = authenticationEntryPoint;}

	/**
	 * Spring: CustomBasicAuthenticationEntryPoint
	 * <p>
	 * sets hardcoded user and pw
	 *
	 * @param auth auth
	 *
	 * @throws Exception see spring
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
		    .withUser("lanteam").password(passwordEncoder().encode("eproisttoll"))
		    .authorities("SWAGGER");
	}

	/**
	 * Spring: configure
	 *
	 * @param http HttpSecurity
	 *
	 * @throws Exception see spring
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.csrf()
				.disable()
				.authorizeRequests()
				.antMatchers(
						"/v2/api-docs",
						"/configuration/ui",
						"/swagger-resources",
						"/configuration/security",
						"/swagger-ui.html",
						"/webjars/**",
						"/swagger-resources/configuration/ui"
				)
				.permitAll()
				.antMatchers("/v2/**").authenticated()
				//.anyRequest().authenticated()
				.and()
				.httpBasic()
				.authenticationEntryPoint(authenticationEntryPoint)
				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	/**
	 * Spring: configure
	 *
	 * @param web WebSecurity
	 */
	@Override
	public void configure(WebSecurity web) {
		web.ignoring()
		   .antMatchers(
				   "/v2/api-docs",
				   "/configuration/ui",
				   "/swagger-resources",
				   "/configuration/security",
				   "/swagger-ui.html",
				   "/webjars/**"
		   );
	}

	/**
	 * Spring: passwordEncoder
	 *
	 * @return passwordEncoder
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}