package com.lanteam.kubernetesapi.server.service;

import com.lanteam.kubernetesapi.server.model.Instance;
import com.lanteam.kubernetesapi.server.util.helper.InstanceFileManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.exceptions.JedisException;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * Class for interacting with an redis service in k8
 */
@Slf4j
@Component
public class K8sRedisService {
	/**
	 * Constances
	 */
	public static class KEYS {
		/**
		 * redis password field
		 */
		public final static String PW = "requirepass";
	}

	/**
	 * Helmservice
	 */
	private final HelmService         helmService;
	/**
	 * instanceService
	 */
	private final InstanceFileManager instanceFileManager;

	/**
	 * DI Constructor
	 *
	 * @param helmService         Helmservice
	 * @param instanceFileManager instanceService
	 */
	public K8sRedisService(HelmService helmService, InstanceFileManager instanceFileManager) {
		this.helmService = helmService;
		this.instanceFileManager = instanceFileManager;
	}

	/**
	 * Proxies all parameters to the redis instance.
	 * <p>
	 * because the service might not be created successful it is moved to an {@link ScheduledExecutorService} (Timer).
	 * To prevent it from running endless a stop condition of 5minutes is implemented. Non Redis Keys get ignored.
	 *
	 * @param instanceId instance
	 * @param parameters parameters
	 */
	public void setParameters(final String instanceId, Map<String, String> parameters) {
		if (parameters == null) {
			return;
		}
		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		final int[]              i        = {0};
		executor.scheduleAtFixedRate(() -> {
			if (this.helmService.isReady(instanceId)) {
				K8sRedisService.log.debug("Pod is ready.");
				Instance instance = instanceFileManager.loadFile().get(instanceId);
				String   pw       = null;
				if (parameters.containsKey(KEYS.PW)) {
					pw = parameters.remove(KEYS.PW);
				}
				try (Jedis jedis = this.getJedis(instanceId)) {
					K8sRedisService.log.debug("Start setting parameters to pod.");
					parameters.forEach((key, value) -> {
						try {
							jedis.configSet(key, value);
							instance.getParameters().put(key, value);
						} catch (JedisException ignored) {
						}
					});
					jedis.configSet(KEYS.PW, pw);
					instance.getParameters().put(KEYS.PW, pw);
				} finally {
					instanceFileManager.putInstance(instanceId, instance);
					executor.shutdown();
				}
			}
			i[0]++;
			if (i[0] == 300) {
				executor.shutdown();
			}
		}, 0, 1000, TimeUnit.MILLISECONDS);
	}


	/**
	 * get a redis client
	 *
	 * @param instanceId instanceId
	 *
	 * @return jedis object
	 */
	private Jedis getJedis(final String instanceId) {
		String password = instanceFileManager.loadFile().get(instanceId).getPassword();
		K8sRedisService.log.debug("Found password:" + password);
		String[] url = this.helmService.getRedisUrl(instanceId).split(":");
		K8sRedisService.log.debug("Found host:" + Arrays.toString(url));
		JedisShardInfo shardInfo = new JedisShardInfo(url[0], Integer.parseInt(url[1]));
		if (password != null && !password.isEmpty()) {
			shardInfo.setPassword(password);
		}
		K8sRedisService.log.debug("ShardInfo-Object created.");
		return new Jedis(shardInfo);
	}
}
