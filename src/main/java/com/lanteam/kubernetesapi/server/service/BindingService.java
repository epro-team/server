package com.lanteam.kubernetesapi.server.service;

import com.lanteam.kubernetesapi.server.model.Credentials;
import com.lanteam.kubernetesapi.server.model.Instance;
import com.lanteam.kubernetesapi.server.model.request.ServiceBindingRequest;
import com.lanteam.kubernetesapi.server.model.response.ServiceBinding;
import com.lanteam.kubernetesapi.server.util.helper.BindingFileManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Binding service
 */
@Component
public class BindingService {

	/**
	 * Helmservice
	 */
	private HelmService        helmService;
	/**
	 * fileManager
	 */
	private BindingFileManager fileManager;
	/**
	 * instanceService
	 */
	private InstanceService    instanceService;

	/**
	 * DI Constructor
	 *
	 * @param helmService     HelmService
	 * @param fileManager     fileManager
	 * @param instanceService InstanceService
	 */
	@Autowired
	public BindingService(HelmService helmService, BindingFileManager fileManager, InstanceService instanceService) {
		this.helmService = helmService;
		this.fileManager = fileManager;
		this.instanceService = instanceService;
	}

	/**
	 * get a binding
	 *
	 * @param instance_id instance id
	 * @param binding_id  binding id
	 *
	 * @return ServiceBinding object
	 */
	public ServiceBindingRequest getBinding(String instance_id, String binding_id) {
		Map<String, ServiceBindingRequest> bindings = fileManager.loadFile().get(instance_id);
		if (bindings == null) {
			return null;
		}
		return bindings.get(binding_id);
	}

	/**
	 * create a new binding
	 *
	 * @param instance_id           instance id
	 * @param binding_id            binding id
	 * @param serviceBindingRequest request
	 *
	 * @return ServiceBinding object
	 */
	public ServiceBinding createBinding(String instance_id, String binding_id, ServiceBindingRequest serviceBindingRequest) {
		fileManager.addBinding(instance_id, binding_id, serviceBindingRequest);
		return new ServiceBinding(getCredentials(instance_id), null, null, null);
	}

	/**
	 * delete a binding
	 *
	 * @param instance_id instance id
	 * @param binding_id  binding id
	 */
	public void deleteBinding(String instance_id, String binding_id) {
		fileManager.removeBinding(instance_id, binding_id);
	}

	/**
	 * get credential information from helmService and instance
	 *
	 * @param instance_id instance id
	 * @return filled credentials
	 */
	public Credentials getCredentials(String instance_id) {
		Instance    instance        = instanceService.getInstance(instance_id);
		Credentials credentials     = new Credentials();
		String[]    redisConnection = helmService.getRedisUrl(instance_id).split(":");
		credentials.setHost(redisConnection[0]);
		credentials.setPort(Integer.parseInt(redisConnection[1]));
		credentials.setPassword(instance.getPassword());
		return credentials;
	}
}
