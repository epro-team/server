package com.lanteam.kubernetesapi.server.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lanteam.kubernetesapi.server.model.Plan;
import com.lanteam.kubernetesapi.server.model.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * service for the catalog
 */
@Component
public class CatalogService {

	/**
	 * services
	 */
	private ArrayList<Service> services;
	/**
	 * objectMapper
	 */
	private ObjectMapper       objectMapper;

	/**
	 * DI Constructor
	 *
	 * @param objectMapper ObjectMapper
	 *
	 * @throws IOException if file cannot found
	 */
	@Autowired
	public CatalogService(ObjectMapper objectMapper) throws IOException {
		this.objectMapper = objectMapper;
		File catalogFile = new File("catalog.json");
		if (!catalogFile.exists()) {
			createDefaultCatalog(catalogFile);
		}
		readCatalog(catalogFile);
	}

	/**
	 * get services
	 *
	 * @return services
	 */
	public List<Service> getServices() {
		return services;
	}

	/**
	 * checks if a service exists
	 *
	 * @param service_id service id
	 *
	 * @return existence of a service
	 */
	public boolean serviceExists(String service_id) {
		return services.stream().anyMatch(s -> s.getId().equals(service_id));
	}

	/**
	 * checks if a plan exists
	 *
	 * @param service_id service id
	 * @param plan_id    plan id
	 *
	 * @return existence of a plan
	 */
	public boolean planExists(String service_id, String plan_id) {
		return services.stream()
		               .filter(s -> s.getId().equals(service_id))
		               .limit(1)
		               .flatMap(s -> s.getPlans().stream())
		               .anyMatch(p -> p.getId().equals(plan_id));
	}

	/**
	 * get the id of a plan and a service
	 *
	 * @param service_id service id
	 * @param planName   plan name / uuid
	 *
	 * @return get plan id
	 */
	public String getPlanId(String service_id, String planName) {
		return services.stream()
		               .filter(s -> s.getId().equals(service_id))
		               .limit(1)
		               .flatMap(s -> s.getPlans().stream())
		               .filter(p -> p.getName().equals(planName))
		               .map(Plan::getId)
		               .findFirst()
		               .orElse("");
	}

	/**
	 * create default json on startup
	 *
	 * @param catalogFile catalog file
	 *
	 * @throws IOException writing is failed
	 */
	private void createDefaultCatalog(File catalogFile) throws IOException {
		objectMapper.writeValue(catalogFile, getDefaultServiceConfig());
	}

	/**
	 * read the catalog from disk
	 *
	 * @param catalogFile place of catalog json
	 *
	 * @throws IOException reading is failed
	 */
	private void readCatalog(File catalogFile) throws IOException {
		services = objectMapper.readValue(catalogFile, new TypeReference<ArrayList<Service>>() {});
	}

	/**
	 * get the default service config on first start
	 *
	 * @return default service config
	 */
	private Service[] getDefaultServiceConfig() {
		Service[] services = new Service[1];
		services[0] = new Service(
				"redis",
				UUID.randomUUID().toString(),
				"Redis database for everyone",
				getTagsForDefaultService(),
				null,
				true,
				true,
				true,
				null,
				null,
				true,
				getDefaultPlans()
		);
		return services;
	}

	/**
	 * get default plans on first start
	 *
	 * @return default plans
	 */
	private ArrayList<Plan> getDefaultPlans() {
		ArrayList<Plan> plans = new ArrayList<>();
		plans.add(new Plan(UUID.randomUUID().toString(), "small", "One pod with few ressources"));
		plans.add(new Plan(UUID.randomUUID().toString(), "standard", "One pod with many ressources"));
		plans.add(new Plan(UUID.randomUUID().toString(), "cluster", "Six pod cluster"));
		return plans;
	}

	/**
	 * get default tags on first start
	 *
	 * @return default list of tags
	 */
	private ArrayList<String> getTagsForDefaultService() {
		ArrayList<String> tags = new ArrayList<>();
		tags.add("redis");
		tags.add("key-value");
		tags.add("database");
		tags.add("db");
		return tags;
	}
}
