package com.lanteam.kubernetesapi.server.service;

import com.lanteam.kubernetesapi.server.exceptions.InternalServerErrorException;
import com.lanteam.kubernetesapi.server.exceptions.NotFoundException;
import com.lanteam.kubernetesapi.server.model.ErrorCode;
import com.lanteam.kubernetesapi.server.model.Operation;
import com.lanteam.kubernetesapi.server.model.OperationState;
import com.lanteam.kubernetesapi.server.model.Plan;
import com.lanteam.kubernetesapi.server.model.response.LastOperation;
import com.lanteam.kubernetesapi.server.util.helper.HelmHelper;
import com.lanteam.kubernetesapi.server.util.helper.UrlHelper;
import hapi.chart.ChartOuterClass.Chart.Builder;
import hapi.release.ReleaseOuterClass;
import hapi.services.tiller.Tiller;
import io.fabric8.kubernetes.api.model.ServicePort;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetStatus;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.microbean.helm.ReleaseManager;
import org.microbean.helm.chart.URLChartLoader;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.lanteam.kubernetesapi.server.util.helper.HelmHelper.KEYS.REDIS_HELM;

/**
 * This service handles the connection und use of the helm api
 */
@Service
@Slf4j
public class HelmService {
	/**
	 * Kubernetes client
	 */
	private final DefaultKubernetesClient   client;
	/**
	 * tiller client
	 */
	private final org.microbean.helm.Tiller tiller;
	/**
	 * release manager helm client
	 */
	private final ReleaseManager            releaseManager;

	/**
	 * Catalog with service definition
	 */
	private final CatalogService catalogService;

	/**
	 * init helm service provider and all connections
	 *
	 * @param catalogService CatalogService
	 *
	 * @throws MalformedURLException master url wrong
	 */
	public HelmService(CatalogService catalogService) throws MalformedURLException {
		this.catalogService = catalogService;
		Config config = new ConfigBuilder().build();

		client = new DefaultKubernetesClient(config);
		tiller = new org.microbean.helm.Tiller(client);
		releaseManager = new ReleaseManager(tiller);
		if (HelmService.log.isDebugEnabled()) {
			HelmService.log.debug("kubernetes master url: {}", config.getMasterUrl());
			HelmService.log.debug("kubernetes api version: {}", client.getApiVersion());
		}
	}

	/**
	 * uninstall a prev installed release from kubernetes using helm / tiller
	 *
	 * @param instanceID name of chart instance
	 * @param method     callback
	 *
	 * @throws IOException Uninstalling failed
	 */
	public void uninstall(final String instanceID, final After method) throws IOException {
		HelmService.log.debug("Starting uninstalling {}", HelmHelper.makeSlug(instanceID));
		final Tiller.UninstallReleaseRequest.Builder builder = Tiller.UninstallReleaseRequest.newBuilder();
		builder.setTimeout(300L);
		builder.setName(HelmHelper.makeSlug(instanceID));
		builder.setPurge(true);


		final Future<Tiller.UninstallReleaseResponse> releaseFuture = releaseManager.uninstall(builder.build());
		HelmService.log.debug("finished uninstalling {}: {}", HelmHelper.makeSlug(instanceID), releaseFuture);
		assert releaseFuture != null;


		this.run(HelmHelper.makeSlug(instanceID), releaseFuture, () -> {
			log.debug("Deleting persistentVolumeClaims...");
				/*
				 Helm doesn't delete persistentVolumeClaims automatically so we delete them manually
				 */
			client
					.persistentVolumeClaims()
					.list()
					.getItems()
					.stream()
					.filter(persistentVolumeClaim -> persistentVolumeClaim
							.getMetadata()
							.getName()
							.contains(HelmHelper.makeSlug(instanceID)))
					.forEach(persistentVolumeClaim -> client
							.persistentVolumeClaims()
							.delete(persistentVolumeClaim));
			method.run();
		}, true);
	}

	/**
	 * get the url to the dashboard of the service
	 *
	 * @param instanceId instance id
	 * @param wait       loop and sleep for guaranteed finding port
	 *
	 * @return dashboard url
	 */
	public String getDashboardUrl(String instanceId, boolean wait) {
		return this.getPublicUrl(HelmHelper.makeSlug(instanceId), "ui", wait);
	}

	/**
	 * get the url to the public url of the service
	 *
	 * @param instanceId instance id
	 *
	 * @return public redis url
	 */
	public String getRedisUrl(String instanceId) {
		return this.getRedisUrl(HelmHelper.makeSlug(instanceId), true);
	}

	/**
	 * get the url to the public url of the service
	 *
	 * @param instanceId instance id
	 * @param wait       loop and sleep for guaranteed finding port
	 *
	 * @return public redis url
	 */
	public String getRedisUrl(String instanceId, boolean wait) {
		return this.getPublicUrl(HelmHelper.makeSlug(instanceId), "public", wait);
	}

	/**
	 * install a plan to an instance
	 *
	 * @param instanceId instance id
	 * @param service_id service id
	 * @param plan_id    plan id
	 * @param method     callback
	 */
	public void install(final String instanceId, final String service_id, final String plan_id, final After method) {
		HelmService.log.debug(
				"Try installing a new plan: service_id: {} plan_id:{} instanceId:{}",
				service_id,
				plan_id,
				HelmHelper.makeSlug(instanceId)
		);
		this.doChartInteraction(
				HelmHelper.makeSlug(instanceId),
				service_id,
				plan_id,
				(iid, yaml, chart) -> {
					final Tiller.InstallReleaseRequest.Builder requestBuilder = Tiller.InstallReleaseRequest.newBuilder();
					requestBuilder.setTimeout(300L);
					requestBuilder.setName(iid);
					requestBuilder.setWait(false);

					final String yamlString = new Yaml().dump(yaml);
					requestBuilder.getValuesBuilder().setRaw(yamlString);


					final Future<Tiller.InstallReleaseResponse> releaseFuture = releaseManager.install(
							requestBuilder,
							chart
					);
					assert releaseFuture != null;
					final ReleaseOuterClass.Release release = releaseFuture.get().getRelease();
					assert release != null;
					this.run(iid, releaseFuture, method);
				}
		);
	}

	/**
	 * upgrades an instance to a new plan
	 *
	 * @param instanceId instance id
	 * @param service_id service id
	 * @param plan_id    plan id
	 * @param method     callback
	 */
	public void upgrade(final String instanceId, final String service_id, final String plan_id, final After method) {
		HelmService.log.debug(
				"Try upgrading to a different plan: service_id: {} plan_id:{} instanceId:{}",
				service_id,
				plan_id,
				HelmHelper.makeSlug(instanceId)
		);
		this.doChartInteraction(
				HelmHelper.makeSlug(instanceId),
				service_id,
				plan_id,
				(iid, yaml, chart) -> {
					HelmService.log.debug("Starting upgrading {}", iid);
					final Tiller.UpdateReleaseRequest.Builder builder = Tiller.UpdateReleaseRequest.newBuilder();
					builder.setTimeout(300L);
					builder.setName(iid);
					final String yamlString = new Yaml().dump(yaml);
					builder.getValuesBuilder().setRaw(yamlString);


					final Future<Tiller.UpdateReleaseResponse> releaseFuture = releaseManager.update(builder, chart);
					HelmService.log.debug("finished upgrading {}: {}", iid, releaseFuture);

					assert releaseFuture != null;

					this.run(iid, releaseFuture, method);
				}
		);
	}

	/**
	 * checks if an instance is ready to use
	 *
	 * @param instanceId instance id
	 *
	 * @return is ready
	 */
	public boolean isReady(String instanceId) {
		instanceId = HelmHelper.makeSlug(instanceId);
		final StatefulSetStatus status = this.getStatefulStatus(instanceId);
		if (status.getReadyReplicas() != null && status.getReplicas() != null) {
			return status.getReplicas().intValue() == status.getReadyReplicas().intValue();
		}
		return false;
	}

	/**
	 * get a state to an instance
	 *
	 * @param instanceId instance id
	 * @param operation  current operation
	 *
	 * @return state
	 */
	public LastOperation getState(String instanceId, final Operation operation) {
		instanceId = HelmHelper.makeSlug(instanceId);

		if (operation.equals(Operation.PROVISION) || operation.equals(Operation.UPDATE)) {
			final StatefulSetStatus status  = this.getStatefulStatus(instanceId);
			String                  message = String.format("%s: Start initialisation", instanceId);
			if (status.getReadyReplicas() != null && status.getReplicas() != null) {
				double Percent = status.getReadyReplicas().doubleValue() / status.getReplicas().doubleValue();
				message = Percent == 1 ?
				          String.format("%s: Finished", HelmHelper.removeSlug(instanceId)) :
				          String.format(
						          "%s: Initialisation is %.2f%% ready",
						          HelmHelper.removeSlug(instanceId),
						          Percent * 100
				          );
			}
			return new LastOperation(
					this.isReady(instanceId) ? OperationState.SUCCEEDED : OperationState.IN_PROGRESS,
					message
			);
		} else {
			final String finalInstanceId = instanceId;
			long count = client
					.pods()
					.list()
					.getItems()
					.stream()
					.filter(pod -> pod.getMetadata().getName().contains(finalInstanceId)).count();
			return new LastOperation(
					count == 0 ? OperationState.SUCCEEDED : OperationState.IN_PROGRESS,
					count == 0 ? String.format("%s: Finished", HelmHelper.removeSlug(instanceId))
					           : String.format("%s: started removing", HelmHelper.removeSlug(instanceId))
			);
		}
	}

	/**
	 * loads a chart from java url object
	 *
	 * @param chartUrl chart url. Local or remote possible
	 *
	 * @return chart
	 *
	 * @throws IOException chart loading failed
	 */
	private Builder loadChart(final URL chartUrl) throws IOException {
		HelmService.log.debug("Loading chart from: {}", chartUrl.toString());
		Builder chart;
		try (URLChartLoader chartLoader = new URLChartLoader()) {
			chart = chartLoader.load(chartUrl);
		}
		HelmService.log.debug("Loading chart: successful");
		return chart;
	}

	/**
	 * this method triggers after the instance was successfully created/update/removed the callback
	 *
	 * @param instanceId    instance
	 * @param releaseFuture task
	 * @param method        callback
	 */
	private void run(final String instanceId, Future<?> releaseFuture, final After method) {
		this.run(instanceId, releaseFuture, method, false);
	}

	/**
	 * this method triggers after the instance was successfully created/update/removed the callback
	 *
	 * @param instanceId    instance
	 * @param releaseFuture task
	 * @param method        callback
	 * @param skipReady     skip ready check (e.g. on uninstalls)
	 */
	private void run(final String instanceId, Future<?> releaseFuture, final After method, boolean skipReady) {
		final String typeName = ((ParameterizedType) releaseFuture
				.getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0].toString();
		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		final int[]              i        = {0};
		executor.scheduleAtFixedRate(() -> {
			if (releaseFuture.isDone()) {
				log.debug("{}: Callback: isDone ready", typeName);
				if (this.isReady(instanceId) || skipReady) {
					log.debug("{}: Callback: isReady ready", typeName);
					method.run();
					executor.shutdown();
				}
			}
			i[0]++;
			if (i[0] == 300) {
				executor.shutdown();
			}
		}, 0, 1000, TimeUnit.MILLISECONDS);
	}

	/**
	 * get the url to the dashboard of the service
	 *
	 * @param instanceId instance id
	 * @param type       port type
	 * @param wait       loop and sleep for guaranteed finding port
	 *
	 * @return dashboard url
	 *
	 * @throws InternalServerErrorException ip localization failed
	 */
	private String getPublicUrl(String instanceId, String type, boolean wait) throws InternalServerErrorException {
		HelmService.log.debug("Start fetching dashboard url for instance: {}", instanceId);
		String ip;
		try {
			ip = new URL(client.getConfiguration().getMasterUrl()).getHost();
		} catch (MalformedURLException e) {
			throw new InternalServerErrorException(ErrorCode.INTERNAL_SERVER_ERROR, "Couldn't reach master k8 server");
		}
		HelmService.log.debug(" * Found ip: {}", ip);
		if (wait) {
			for (int i = 0; i < 50; i++) {
				try {
					int port = this.getPort(instanceId, type);
					Thread.sleep(100);
					if (port != 0) {
						HelmService.log.debug(" * Found port: {}", port);
						return ip + ":" + port;
					}
				} catch (InterruptedException e) {
					HelmService.log.debug("Sleep Exception", e);
				}
			}
			HelmService.log.debug("No port found.");
			throw new NotFoundException(ErrorCode.PORT_NOT_FOUND, "No port for given instance detected");
		} else {
			int port = this.getPort(instanceId, type);
			HelmService.log.debug(" * Found port: {}", port);
			if (port == 0) {
				throw new NotFoundException(ErrorCode.PORT_NOT_FOUND, "No port for given instance detected");
			}
			return ip + ":" + port;
		}
	}

	/**
	 * get port or 0 of an instance
	 *
	 * @param instanceId instance id
	 * @param type       service type
	 *
	 * @return port or 0
	 */
	private Integer getPort(String instanceId, String type) {
		return client
				.services()
				.list()
				.getItems()
				.stream()
				.filter(service -> service
						.getMetadata()
						.getName()
						.contains(instanceId + "-redis-" + type))
				.flatMap(service -> service.getSpec().getPorts().stream())
				.filter(servicePort -> servicePort.getName().equals("http"))
				.map(ServicePort::getNodePort)
				.findFirst()
				.orElse(0);
	}

	/**
	 * Standardises the interaction with helms chart feature
	 * <p>
	 * Logs necessary data and receive correct plan automatic
	 *
	 * @param instanceId instance id
	 * @param service_id service id
	 * @param plan_id    plan id
	 * @param method     method which interacts with load chart
	 */
	private void doChartInteraction(final String instanceId, final String service_id, final String plan_id, final ChartInteraction method) {
		try {
			Plan plan = this.getPlanFromCatalog(service_id, plan_id);

			HelmService.log.debug("Found matching plan: {}", plan.getName());
			File file = new File("./helm/charts/redis");
			if (!file.isDirectory()) {
				InputStream initialStream = HelmService.class.getResourceAsStream(REDIS_HELM);
				file = new File(REDIS_HELM);
				FileUtils.copyInputStreamToFile(initialStream, file);
			}

			final Builder             chart  = this.loadChart(UrlHelper.getLocalChart(file));
			final Map<String, Object> config = new HashMap<>();
			config.put("Variation", plan.getName());
			HelmService.log.debug("Start interacting");
			method.run(instanceId, config, chart);
		} catch (Exception e) {
			throw new UnsupportedOperationException(e);
		}
	}

	/**
	 * get the plan from it's id
	 *
	 * @param serviceId service id
	 * @param planId    plan id
	 *
	 * @return plan object
	 */
	private Plan getPlanFromCatalog(final String serviceId, final String planId) throws IllegalArgumentException {
		return this.catalogService
				.getServices()
				.stream()
				.filter(service -> service.getId().equals(serviceId))
				.flatMap(service -> service.getPlans().stream())
				.filter(plan -> plan.getId().equals(planId))
				.findFirst()
				.orElseThrow(IllegalArgumentException::new);
	}

	/**
	 * get stateful kubernetes object
	 *
	 * @param instanceId Instance Id
	 *
	 * @return stateful
	 */
	private StatefulSetStatus getStatefulStatus(final String instanceId) {
		return this.client
				.apps()
				.statefulSets()
				.list()
				.getItems()
				.stream()
				.filter(statefulSet -> statefulSet.getMetadata().getName().contains(instanceId + "-redis"))
				.findFirst()
				.orElse(new StatefulSet(null, null, null, null, new StatefulSetStatus()))
				.getStatus();
	}

	/**
	 * Close all connections
	 * <p>
	 * close helmclient, tiller and release manager connection
	 *
	 * @throws IOException closing connection failed
	 */
	@PreDestroy
	public void close() throws IOException {
		releaseManager.close();
		tiller.close();
		client.close();
	}

	/**
	 * Interface for lambda usage
	 */
	interface ChartInteraction {
		void run(String instanceId, Map<String, Object> config, Builder chart) throws Exception;
	}

	/**
	 * Interface for lambda usage
	 */
	interface After {
		void run();
	}
}

