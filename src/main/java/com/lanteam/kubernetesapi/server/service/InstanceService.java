package com.lanteam.kubernetesapi.server.service;

import com.lanteam.kubernetesapi.server.exceptions.BadRequestException;
import com.lanteam.kubernetesapi.server.exceptions.InternalServerErrorException;
import com.lanteam.kubernetesapi.server.exceptions.NotFoundException;
import com.lanteam.kubernetesapi.server.model.ErrorCode;
import com.lanteam.kubernetesapi.server.model.Instance;
import com.lanteam.kubernetesapi.server.model.Operation;
import com.lanteam.kubernetesapi.server.model.OperationState;
import com.lanteam.kubernetesapi.server.model.request.ProvisionServiceInstanceRequestBody;
import com.lanteam.kubernetesapi.server.model.request.ServiceInstanceUpdateRequest;
import com.lanteam.kubernetesapi.server.model.response.LastOperation;
import com.lanteam.kubernetesapi.server.model.response.ServiceInstanceResource;
import com.lanteam.kubernetesapi.server.util.helper.InstanceFileManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

/**
 * InstanceService for CRUD instances
 */
@Slf4j
@Component
public class InstanceService {
	/**
	 * HelmService
	 */
	private HelmService         helmService;
	/**
	 * KubernetesRedisService
	 */
	private K8sRedisService     k8SRedisService;
	/**
	 * InstanceFileManager
	 */
	private InstanceFileManager instanceFileManager;

	/**
	 * DI Constructor
	 *
	 * @param helmService         HelmService
	 * @param k8SRedisService      K8RedisService
	 * @param instanceFileManager InstanceFileManager
	 */
	@Autowired
	public InstanceService(HelmService helmService, K8sRedisService k8SRedisService, InstanceFileManager instanceFileManager) {
		this.helmService = helmService;
		this.k8SRedisService = k8SRedisService;
		this.instanceFileManager = instanceFileManager;
	}

	/**
	 * provision / deploy an instance
	 *
	 * @param instance_id instance id
	 * @param requestBody http body data
	 *
	 * @return new Instance object
	 */
	public Instance provisionInstance(String instance_id, ProvisionServiceInstanceRequestBody requestBody) {
		Instance instance = getInstance(instance_id);
		if (instance == null) {
			helmService.install(instance_id, requestBody.getService_id(), requestBody.getPlan_id(), () -> {
				log.debug("Start trigger setParameters");
				k8SRedisService.setParameters(instance_id, requestBody.getParameters());
			});
			String dashboardUrl = helmService.getDashboardUrl(instance_id, true);
			instance = new Instance(
					requestBody.getService_id(),
					requestBody.getPlan_id(),
					null,
					requestBody.getContext(),
					requestBody.getOrganization_guid(),
					requestBody.getSpace_guid(),
					requestBody.getParameters(),
					new HashMap<>(),
					dashboardUrl,
					Operation.PROVISION.name() + "_" + UUID.randomUUID(),
					null,
					null
			);
			instanceFileManager.putInstance(instance_id, instance);
		}
		return instance;
	}

	/**
	 * update the plan or parameter of an instance
	 *
	 * @param instance_id instance id
	 * @param requestBody http body data
	 *
	 * @return current instance
	 */
	public Instance updateInstance(String instance_id, ServiceInstanceUpdateRequest requestBody) {
		Instance instance = getInstance(instance_id);
		if (instance != null) {
			if (!requestBody.getPlan_id().isEmpty() && !instance.getPlan_id().equals(requestBody.getPlan_id())) {
				helmService.upgrade(instance_id, requestBody.getService_id(), requestBody.getPlan_id(), () -> {
					log.debug("Start trigger setParameters");
					k8SRedisService.setParameters(instance_id, requestBody.getParameters());

					Instance updInstance = getInstance(instance_id);
					updInstance.setDashboard_url(helmService.getDashboardUrl(instance_id, true));
					instanceFileManager.putInstance(instance_id, updInstance);
				});
				// on plan change, the parameters will be lost on redis pod, so set parameters back to an empty hash map
				instance.setParameters(new HashMap<>());
				instance.setPlan_id(requestBody.getPlan_id());
			} else {
				k8SRedisService.setParameters(instance_id, requestBody.getParameters());
			}
			instance.setRequestedParameters(requestBody.getParameters());
			instance.setLastUpdateOperation(Operation.UPDATE.name() + "_" + UUID.randomUUID());

			instanceFileManager.putInstance(instance_id, instance);
		} else {
			throw new BadRequestException(ErrorCode.INSTANCE_NOT_FOUND, "The given instance_id does not exist");
		}
		return instance;
	}

	/**
	 * deprovision / undeploy an instance
	 *
	 * @param instance_id instance id
	 *
	 * @return old Instance
	 */
	public Instance deprovisionInstance(String instance_id) {
		Instance instance = getInstance(instance_id);
		try {
			helmService.uninstall(instance_id, () -> {});
			instance.setLastDeprovisionOperation(Operation.DEPROVISION.name() + "_" + UUID.randomUUID());
			instanceFileManager.putInstance(instance_id, instance);
		} catch (IOException e) {
			throw new InternalServerErrorException(
					ErrorCode.INTERNAL_SERVER_ERROR,
					"Something went wrong in deprovisioning instance. Please retry it later"
			);
		}
		return instance;
	}

	/**
	 * Get data of an instance
	 *
	 * @param instance_id instance id
	 *
	 * @return instance data
	 */
	public ServiceInstanceResource getInstanceResource(String instance_id) {
		Instance instance = getInstance(instance_id);
		if (instance != null) {
			return new ServiceInstanceResource(
					instance.getService_id(),
					instance.getPlan_id(),
					instance.getDashboard_url(),
					instance.getParameters()
			);
		} else {
			throw new NotFoundException(ErrorCode.INSTANCE_NOT_FOUND, "The given instance_id does not exist");
		}
	}

	/**
	 * Get the last operation @{link LastOperation} of an given instance
	 *
	 * @param instance_id instance id
	 * @param operation   a provided identifier for the operation
	 *
	 * @return Last Operation
	 */
	public LastOperation lastOperationInstance(String instance_id, String operation) {
		Instance instance = getInstance(instance_id);
		if (instance == null) {
			throw new BadRequestException(ErrorCode.INSTANCE_NOT_FOUND, "The given instance_id does not exist");
		}
		Operation operationType = instance.getLastProvisionOperation() != null ?
		              Operation.PROVISION :
		              (instance.getLastUpdateOperation() != null ? Operation.UPDATE : Operation.DEPROVISION);
		LastOperation lastOperation = helmService.getState(instance_id, operationType);

		if (lastOperation != null && lastOperation.getState() == OperationState.SUCCEEDED) {
			switch (operationType) {
				case PROVISION:
					instance.setLastProvisionOperation(null);
					instanceFileManager.putInstance(instance_id, instance);
					break;
				case UPDATE:
					instance.setLastUpdateOperation(null);
					instanceFileManager.putInstance(instance_id, instance);
					break;
				case DEPROVISION: // if a deprovision request successful ended, remove instance
					instanceFileManager.removeInstance(instance_id);
					break;
			}
		}
		return lastOperation;
	}

	/**
	 * get an instance
	 *
	 * @param instance_id instance id
	 *
	 * @return new instance object
	 */
	public Instance getInstance(String instance_id) {
		return instanceFileManager.loadFile().get(instance_id);
	}
}
