package com.lanteam.kubernetesapi.server.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum OperationState {
	IN_PROGRESS("in progress"),
	SUCCEEDED("succeeded"),
	FAILED("failed");

	private String state;

	@JsonValue
	String getState() {
		return state;
	}
}
