package com.lanteam.kubernetesapi.server.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceBindingResourceObject implements Serializable {
	private String app_guid;
	private String route;
}
