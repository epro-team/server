package com.lanteam.kubernetesapi.server.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceBindingVolumeMountDevice implements Serializable {
	@NonNull
	@ApiModelProperty(required = true)
	private String volume_id;
	private Object mount_config;
}
