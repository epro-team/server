package com.lanteam.kubernetesapi.server.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum VolumeMode {
	MODE_R("r"),
	MODE_RW("rw");

	private String name;

	@JsonValue
	public String getName() {
		return name;
	}
}
