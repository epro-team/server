package com.lanteam.kubernetesapi.server.model.response;

import com.lanteam.kubernetesapi.server.model.Credentials;
import com.lanteam.kubernetesapi.server.model.ServiceBindingVolumeMount;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@RequiredArgsConstructor
@Data
public class ServiceBinding implements Serializable {
	private Credentials               credentials;
	private String                    syslog_drain_url;
	private String                    route_service_url;
	private ServiceBindingVolumeMount volume_mounts;
}
