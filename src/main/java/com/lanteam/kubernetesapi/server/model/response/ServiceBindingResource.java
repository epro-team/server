package com.lanteam.kubernetesapi.server.model.response;

import com.lanteam.kubernetesapi.server.model.ServiceBindingVolumeMount;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@RequiredArgsConstructor
@Data
public class ServiceBindingResource implements Serializable {
	private Object                    credentials;
	private String                    syslog_drain_url;
	private String                    route_service_url;
	private ServiceBindingVolumeMount volume_mounts;
	private Object                    parameters;
}
