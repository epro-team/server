package com.lanteam.kubernetesapi.server.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceInstanceSchema implements Serializable {
	private InputParametersSchema create;
	private InputParametersSchema update;
}
