package com.lanteam.kubernetesapi.server.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DashboardUrlResponse {
	private String dashboard_url;
}
