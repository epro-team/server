package com.lanteam.kubernetesapi.server.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum DeviceType {
	TYPE_SHARED("shared");

	private String name;

	@JsonValue
	public String getName() {
		return name;
	}
}
