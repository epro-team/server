package com.lanteam.kubernetesapi.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lanteam.kubernetesapi.server.service.K8sRedisService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Instance {

    private String service_id;
    private String plan_id;
    private String previous_plan;
    private Object context;
    private String organization_guid;
    private String space_guid;
    private Map<String, String> requestedParameters;
    private Map<String, String> parameters;
    private String dashboard_url;
    private String lastProvisionOperation;
    private String lastUpdateOperation;
    private String lastDeprovisionOperation;

	@JsonIgnore
	public String getPassword() {
		return getParameters().getOrDefault(K8sRedisService.KEYS.PW, "");
	}
}
