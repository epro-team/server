package com.lanteam.kubernetesapi.server.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.lanteam.kubernetesapi.server.model.OperationState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LastOperation implements Serializable {
	OperationState state;
	String         description;
}
