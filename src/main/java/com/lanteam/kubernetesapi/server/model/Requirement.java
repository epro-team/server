package com.lanteam.kubernetesapi.server.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Requirement {
	ROUTE_FORWARDING("route_forwarding"),
	SYSLOG_DRAIN("syslog_drain"),
	VOLUME_MOUNT("volume_mount");

	private String name;

	@JsonValue
	public String getName() {
		return name;
	}
}
