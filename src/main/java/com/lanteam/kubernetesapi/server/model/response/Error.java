package com.lanteam.kubernetesapi.server.model.response;

import com.lanteam.kubernetesapi.server.model.ErrorCode;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;

@ApiModel
@Data
public class Error implements Serializable {
	@NonNull
	ErrorCode error;
	@NonNull
	String    description;
}
