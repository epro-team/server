package com.lanteam.kubernetesapi.server.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ServiceInstanceResource implements Serializable {
	private String service_id;
	private String plan_id;
	private String dashboard_url;
	private Object parameters;
}
