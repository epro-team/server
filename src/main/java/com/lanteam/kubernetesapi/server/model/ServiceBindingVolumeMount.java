package com.lanteam.kubernetesapi.server.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceBindingVolumeMount implements Serializable {
	@NonNull
	@ApiModelProperty(required = true)
	private String                          driver;
	@NonNull
	@ApiModelProperty(required = true)
	private String                          container_dir;
	@NonNull
	@ApiModelProperty(required = true)
	private VolumeMode                      mode;
	@NonNull
	@ApiModelProperty(required = true)
	private DeviceType                      device_type;
	@NonNull
	@ApiModelProperty(required = true)
	private ServiceBindingVolumeMountDevice device;
}
