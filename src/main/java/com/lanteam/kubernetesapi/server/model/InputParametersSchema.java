package com.lanteam.kubernetesapi.server.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InputParametersSchema implements Serializable {
	private Map<String, String> parameters;
}
