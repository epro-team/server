package com.lanteam.kubernetesapi.server.model.response;

import com.lanteam.kubernetesapi.server.model.Service;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@RequiredArgsConstructor
@Data
public class Catalog implements Serializable {
	@NonNull
	private List<Service> services;
}
