package com.lanteam.kubernetesapi.server.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceInstancePreviousValues implements Serializable {
	@Deprecated
	private String service_id;
	private String plan_id;
	@Deprecated
	private String organization_id;
	@Deprecated
	private String space_id;
}