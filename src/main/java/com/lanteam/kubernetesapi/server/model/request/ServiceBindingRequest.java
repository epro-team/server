package com.lanteam.kubernetesapi.server.model.request;

import com.lanteam.kubernetesapi.server.model.ServiceBindingResourceObject;
import lombok.*;

import java.util.Map;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class ServiceBindingRequest {
	private Object context;
	@NonNull
	private String service_id;
	@NonNull
	private String plan_id;
	private String app_guid;
	ServiceBindingResourceObject bind_resource;
	private Map<String, String> parameters;
}
