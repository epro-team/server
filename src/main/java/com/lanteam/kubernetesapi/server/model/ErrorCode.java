package com.lanteam.kubernetesapi.server.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ErrorCode {
	//Default error codes
	ASYNC_REQUIRED("AsyncRequired"),
	CONCURRENCY_ERROR("ConcurrencyError"),
	REQUIRES_APP("RequiresApp"),

    //Now custom error codes
    API_LEVEL_UNSUPPORTED("ApiLevelUnsupported"),
    BAD_REQUEST("BadRequest"),
    BINDING_MISSING("BindingMissing"),
    BINDING_NOT_FOUND("BindingNotFound"),
    DUPLICATE_BINDING("DuplicateBinding"),
    DUPLICATE_INSTANCE("DuplicateInstance"),
    INCOMPATIBLE_PLAN("IncompatiblePlan"),
    INCOMPATIBLE_SERVICE("IncompatibleService"),
    INSTANCE_BUSY("InstanceBusy"),
    INSTANCE_MISSING("InstanceMissing"),
    INSTANCE_NOT_FOUND("InstanceNotFound"),
    INSTANCE_GONE("InstanceAlreadyGone"),
    INTERNAL_SERVER_ERROR("InternalServerError"),
    MISSING_FIELD_IN_HEADER("HeaderIsMissingRequiredField"),
    ORIGINATING_IDENTITY_INVALID("OriginatingIdentityInvalid"),
    PORT_NOT_FOUND("PortNotFound"),
    SERVICE_NOT_FOUND("ServiceNotFound");

	private final String s;

	@Override
	@JsonValue
	public String toString() {
		return this.s;
	}
}
