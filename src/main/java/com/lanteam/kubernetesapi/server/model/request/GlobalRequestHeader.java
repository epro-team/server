package com.lanteam.kubernetesapi.server.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class GlobalRequestHeader {
	@NonNull
	private String xBrokerApiVersion;
	private String xBrokerApiOriginatingIdentity;
}
