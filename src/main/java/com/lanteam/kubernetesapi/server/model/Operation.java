package com.lanteam.kubernetesapi.server.model;

public enum Operation {
	PROVISION,
	UPDATE,
	DEPROVISION,
	BINDING,
	UNBINDING
}
