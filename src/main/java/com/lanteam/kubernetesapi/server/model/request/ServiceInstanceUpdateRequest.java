package com.lanteam.kubernetesapi.server.model.request;

import com.lanteam.kubernetesapi.server.model.ServiceInstancePreviousValues;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Map;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class ServiceInstanceUpdateRequest {
	private Object                        context;
	@NonNull
	@ApiModelProperty(required = true)
	private String                        service_id;
	private String                        plan_id;
	private Map<String, String>           parameters;
	private ServiceInstancePreviousValues previous_values;
}