package com.lanteam.kubernetesapi.server.model.request;

import lombok.*;

import java.util.Map;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class ProvisionServiceInstanceRequestBody {
	@NonNull
	private String              service_id;
	@NonNull
	private String              plan_id;
	private Object              context;
	@NonNull
	private String              organization_guid;
	@NonNull
	private String              space_guid;
	private Map<String, String> parameters;
}
