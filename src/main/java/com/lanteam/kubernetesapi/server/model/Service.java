package com.lanteam.kubernetesapi.server.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;

@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Service implements Serializable {
	@NonNull
	@ApiModelProperty(required = true)
	private String                 name;
	@NonNull
	@ApiModelProperty(required = true)
	private String                 id;
	@NonNull
	@ApiModelProperty(required = true)
	private String                 description;
	private ArrayList<String>      tags;
	private ArrayList<Requirement> requires;
	@NonNull
	@ApiModelProperty(required = true)
	private Boolean                bindable;
	private Boolean                instances_retrievable;
	private Boolean                bindings_retrievable;
	private Object                 metadata;
	private Object                 dashboard_client;
	private Boolean                plan_updateable;
	@NonNull
	@ApiModelProperty(required = true)
	private ArrayList<Plan>        plans;
}
