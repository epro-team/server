package com.lanteam.kubernetesapi.server.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmptyResponse implements Serializable {

    private String dummyField;
}
