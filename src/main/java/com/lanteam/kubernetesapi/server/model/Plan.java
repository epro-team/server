package com.lanteam.kubernetesapi.server.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@Data
@RequiredArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Plan implements Serializable {

	@NonNull
	@ApiModelProperty(required = true)
	private String  id;
	@NonNull
	@ApiModelProperty(required = true)
	private String  name;
	@NonNull
	@ApiModelProperty(required = true)
	private String  description;
	private Object  metadata;
	private Boolean free = true;
	private Boolean bindable;
	private Schemas schemas;

}
