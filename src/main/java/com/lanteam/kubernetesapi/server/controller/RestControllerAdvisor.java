package com.lanteam.kubernetesapi.server.controller;

import com.lanteam.kubernetesapi.server.exceptions.OpenServiceBrokerException;
import com.lanteam.kubernetesapi.server.model.ErrorCode;
import com.lanteam.kubernetesapi.server.model.response.Error;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Generic Exception handler
 */
@RestControllerAdvice
public class RestControllerAdvisor {

	/**
	 * Handle generic all exception that wasn't fetched by this service
	 *
	 * @param e exception
	 *
	 * @return Error
	 */
	@ExceptionHandler(OpenServiceBrokerException.class)
	ResponseEntity<Error> handleOpenServiceBrokerException(OpenServiceBrokerException e) {
		return new ResponseEntity<>(new Error(e.getErrorCode(), e.getMessage()), e.getHttpStatus());
	}

	/**
	 * Handle generic all exception when a requested header is missing
	 *
	 * @param e exception
	 *
	 * @return Error
	 */
	@ExceptionHandler(MissingRequestHeaderException.class)
	ResponseEntity<Error> handleMissingRequestHeaderException(MissingRequestHeaderException e) {
		HttpStatus httpStatus = "X-Broker-API-Version".equals(e.getHeaderName()) ? HttpStatus.PRECONDITION_FAILED : HttpStatus.BAD_REQUEST;
		return new ResponseEntity<>(new Error(ErrorCode.MISSING_FIELD_IN_HEADER, e.getMessage()), httpStatus);
	}

	/**
	 * Handle generic all exception when a requested body element is missing
	 *
	 * @param e exception
	 *
	 * @return Error
	 */
	@ExceptionHandler(HttpMessageNotReadableException.class)
	ResponseEntity<Error> handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
		return new ResponseEntity<>(new Error(ErrorCode.BAD_REQUEST, e.getMessage()), HttpStatus.BAD_REQUEST);
	}
}
