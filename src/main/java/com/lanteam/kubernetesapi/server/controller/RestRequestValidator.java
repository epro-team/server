package com.lanteam.kubernetesapi.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lanteam.kubernetesapi.server.exceptions.*;
import com.lanteam.kubernetesapi.server.model.ErrorCode;
import com.lanteam.kubernetesapi.server.model.Instance;
import com.lanteam.kubernetesapi.server.model.Operation;
import com.lanteam.kubernetesapi.server.model.request.GlobalRequestHeader;
import com.lanteam.kubernetesapi.server.model.request.ProvisionServiceInstanceRequestBody;
import com.lanteam.kubernetesapi.server.model.request.ServiceBindingRequest;
import com.lanteam.kubernetesapi.server.model.request.ServiceInstanceUpdateRequest;
import com.lanteam.kubernetesapi.server.service.BindingService;
import com.lanteam.kubernetesapi.server.service.CatalogService;
import com.lanteam.kubernetesapi.server.service.InstanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Objects;
import java.util.StringTokenizer;

/**
 * Global rest request validator
 */
@Component
public class RestRequestValidator {

	/**
	 * CatalogService: Get a defined catalog with services
	 */
	private       CatalogService  catalogService;
	/**
	 * InstanceService: set of instance functions
	 */
	private       InstanceService instanceService;
	/**
	 * BindingService: set of binding functions
	 */
	private       BindingService  bindingService;
	/**
	 * The magical object mapper
	 */
	private final ObjectMapper    objectMapper;

	/**
	 * DI Constructor
	 *
	 * @param catalogService  CatalogService
	 * @param instanceService InstanceService
	 * @param bindingService  bindingService
	 * @param objectMapper    ObjectMapper
	 */
	@Autowired
	public RestRequestValidator(CatalogService catalogService, InstanceService instanceService, BindingService bindingService, ObjectMapper objectMapper) {
		this.catalogService = catalogService;
		this.instanceService = instanceService;
		this.bindingService = bindingService;
		this.objectMapper = objectMapper;
	}

	/**
	 * validate header information
	 *
	 * @param header global used request header
	 */
	public void validateHeader(GlobalRequestHeader header) {
		validateApiVersion(header.getXBrokerApiVersion());
		validateOriginatingIdentity(header.getXBrokerApiOriginatingIdentity());
	}

	/**
	 * validate the given request parameters for getting the status of an service instance (LastOperation)
	 *
	 * @param instance_id instance id
	 * @param service_id  service id
	 * @param plan_id     plan id
	 * @param operation   last operation id
	 */
	public void validateGetLastOperationOfServiceInstanceRequest(String instance_id, String service_id, String plan_id, String operation) {
		if (instanceService.getInstance(instance_id) == null && (operation == null || !operation.contains(Operation.DEPROVISION
				                                                                                                  .name()))) {
			throw new BadRequestException(ErrorCode.INSTANCE_NOT_FOUND, "The given instance_id does not exist");
		}
		if (operation != null) {

			String   lastOperation = null;
			Instance instance      = instanceService.getInstance(instance_id);

			if (operation.contains(Operation.DEPROVISION.name())) {
				if (instance == null) {
					throw new ElementGoneException(ErrorCode.INSTANCE_GONE, "The instance is already deleted");
				} else {
					lastOperation = instance.getLastDeprovisionOperation();
				}
			} else if (operation.contains(Operation.UPDATE.name())) {
				lastOperation = instance.getLastUpdateOperation();
			} else if (operation.contains(Operation.PROVISION.name())) {
				lastOperation = instance.getLastProvisionOperation();
			}

			if (!operation.equals(lastOperation)) {
				throw new BadRequestException(
						ErrorCode.BAD_REQUEST,
						"This operation was not initiated on this instance_id"
				);
			}
		}
		if (service_id != null && !service_id.equals(instanceService.getInstance(instance_id).getService_id())) {
			if (!service_id.equals(instanceService.getInstance(instance_id).getService_id())) {
				throw new BadRequestException(
						ErrorCode.INCOMPATIBLE_SERVICE,
						"The given service_id is not associated with the given instance_id"
				);
			}
		}
		if (instanceService.getInstance(instance_id).getLastUpdateOperation() != null) {
			if (!plan_id.equals(instanceService.getInstance(instance_id).getPrevious_plan())) {
				throw new BadRequestException(
						ErrorCode.INCOMPATIBLE_PLAN,
						"The given plan_id is not associated with the given instance_id"
				);
			}
		} else {
			if (plan_id != null && !plan_id.equals(instanceService.getInstance(instance_id).getPlan_id())) {
				throw new BadRequestException(
						ErrorCode.INCOMPATIBLE_PLAN,
						"The given plan_id is not associated with the given instance_id"
				);
			}
		}
	}

	/**
	 * validate the given request parameters for creating a service instance
	 *
	 * @param instance_id      instance id
	 * @param requestBody      request body
	 * @param acceptIncomplete allow asynchronous responses
	 */
	public void validateProvisionServiceInstanceRequest(String instance_id, ProvisionServiceInstanceRequestBody
			requestBody, boolean acceptIncomplete) {
		if (instance_id == null || instance_id.isEmpty()) {
			throw new BadRequestException(ErrorCode.INSTANCE_MISSING, "This instance_id is not set");
		}
		validateRequiredAsyncSupport(acceptIncomplete);
		if (!catalogService.serviceExists(requestBody.getService_id())) {
			throw new BadRequestException(
					ErrorCode.SERVICE_NOT_FOUND,
					"The service_id does not exist in the catalog"
			);
		}
		if (!catalogService.planExists(requestBody.getService_id(), requestBody.getPlan_id())) {
			throw new BadRequestException(
					ErrorCode.INCOMPATIBLE_PLAN,
					"The plan_id does not exist in the catalog for this service_id"
			);
		}
		if (requestBody.getOrganization_guid() == null || requestBody.getOrganization_guid().isEmpty()) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST, "The organization_guid must be set");
		}
		if (requestBody.getSpace_guid() == null || requestBody.getSpace_guid().isEmpty()) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST, "The space_guid must be set");
		}
		validateInstanceConflictFree(instance_id, requestBody);
	}

	/**
	 * validate the given request parameters for getting a service instance information
	 *
	 * @param instance_id instance id
	 */
	public void validateGetServiceInstanceRequest(String instance_id) {
		if (instanceService.getInstance(instance_id) == null) {
			throw new NotFoundException(ErrorCode.INSTANCE_NOT_FOUND, "The given instance_id does not exist");
		}
		if (instanceService.getInstance(instance_id).getLastProvisionOperation() != null) {
			throw new NotFoundException(
					ErrorCode.INSTANCE_BUSY,
					"The given instance has not finished starting up yet"
			);
		}
		if (instanceService.getInstance(instance_id).getLastUpdateOperation() != null) {
			throw new UnprocessableEntityException(
					ErrorCode.INSTANCE_BUSY,
					"The instance is currently updated, cant get instance now"
			);
		}
	}

	/**
	 * validate the given request parameters for updating a service instance
	 *
	 * @param instance_id      instance id
	 * @param requestBody      request body
	 * @param acceptIncomplete allow asynchronous responses
	 */
	public void validateUpdateServiceInstanceRequest(String instance_id, ServiceInstanceUpdateRequest
			requestBody, boolean acceptIncomplete) {
		validateRequiredAsyncSupport(acceptIncomplete);
		if (instanceService.getInstance(instance_id) == null) {
			throw new BadRequestException(ErrorCode.INSTANCE_NOT_FOUND, "The given instance_id does not exist");
		}
		if (!catalogService.serviceExists(requestBody.getService_id())) {
			throw new BadRequestException(ErrorCode.SERVICE_NOT_FOUND, "The service_id does not exist in the catalog");
		}
		if (requestBody.getPlan_id() != null) {
			if (!catalogService.planExists(requestBody.getService_id(), requestBody.getPlan_id())) {
				throw new BadRequestException(
						ErrorCode.INCOMPATIBLE_PLAN,
						"The plan_id does not exist in the catalog for this service_id"
				);
			}
			String clusterId = catalogService.getPlanId(requestBody.getService_id(), "cluster");
			if (!instanceService.getInstance(instance_id).getPlan_id().equals(requestBody.getPlan_id())) {
				if (instanceService.getInstance(instance_id).getPlan_id().equals(clusterId)
				    || requestBody.getPlan_id().equals(clusterId)) {
					throw new BadRequestException(
							ErrorCode.BAD_REQUEST,
							"You can only switch between the plans small and standard"
					);
				}
			}
		}

		if (requestBody.getPrevious_values() != null) {
			if (requestBody.getPrevious_values().getPlan_id() != null && !requestBody
					.getPrevious_values()
					.getPlan_id()
					.equals(instanceService.getInstance(instance_id).getPlan_id())) {
				throw new BadRequestException(ErrorCode.BAD_REQUEST, "Incorrect previous plan_id given");
			}
			if (requestBody.getPrevious_values().getOrganization_id() != null && !requestBody
					.getPrevious_values()
					.getOrganization_id()
					.equals(instanceService.getInstance(instance_id).getOrganization_guid())) {
				throw new BadRequestException(ErrorCode.BAD_REQUEST, "Incorrect previous organization_id given");
			}
			if (requestBody.getPrevious_values().getService_id() != null && !requestBody
					.getPrevious_values()
					.getService_id()
					.equals(instanceService.getInstance(instance_id).getService_id())) {
				throw new BadRequestException(ErrorCode.BAD_REQUEST, "Incorrect previous service_id given");
			}
			if (requestBody.getPrevious_values().getSpace_id() != null && !requestBody
					.getPrevious_values()
					.getSpace_id()
					.equals(instanceService.getInstance(instance_id).getSpace_guid())) {
				throw new BadRequestException(ErrorCode.BAD_REQUEST, "Incorrect previous space_id given");
			}
		}
		if (checkForDifferentActiveOperation(instance_id, Operation.UPDATE)) {
			throw new UnprocessableEntityException(
					ErrorCode.CONCURRENCY_ERROR,
					"The instance is already in an active operation."
			);
		}
		if (instanceService.getInstance(instance_id).getLastUpdateOperation() != null) {
			if (!(Objects.equals(
					instanceService.getInstance(instance_id).getRequestedParameters(),
					requestBody.getParameters()
			) &&
			      Objects.equals(
					      instanceService.getInstance(instance_id).getPlan_id(),
					      requestBody.getPlan_id()
			      ))) {
				throw new UnprocessableEntityException(
						ErrorCode.CONCURRENCY_ERROR,
						"The instance is already being updated"
				);
			}
		}
	}

	/**
	 * validate the given request parameters for deleting a service instance
	 *
	 * @param instance_id      instance id
	 * @param service_id       service id
	 * @param plan_id          plan id
	 * @param acceptIncomplete allow asynchronous responses
	 */
	public void validateDeleteServiceInstanceRequest(String instance_id, String service_id, String plan_id,
	                                                 boolean acceptIncomplete) {
		validateRequiredAsyncSupport(acceptIncomplete);
		if (instance_id == null || instance_id.isEmpty()) {
			throw new BadRequestException(ErrorCode.INSTANCE_MISSING, "This instance_id is not set");
		}
		if (instanceService.getInstance(instance_id) == null) {
			throw new ElementGoneException(ErrorCode.INSTANCE_GONE, "The instance is already deleted");
		}
		if (!instanceService.getInstance(instance_id).getService_id().equals(service_id)) {
			throw new BadRequestException(
					ErrorCode.INCOMPATIBLE_SERVICE,
					"The given service_id is not associated with the given instance_id"
			);
		}
		if (!instanceService.getInstance(instance_id).getPlan_id().equals(plan_id)) {
			throw new BadRequestException(
					ErrorCode.INCOMPATIBLE_PLAN,
					"The given plan_id is not associated with the given instance_id"
			);
		}
		if (checkForDifferentActiveOperation(instance_id, Operation.DEPROVISION)) {
			throw new UnprocessableEntityException(
					ErrorCode.CONCURRENCY_ERROR,
					"The instance is already in an active operation."
			);
		}
	}

	/**
	 * validate the given request parameters for generating a service binding
	 *
	 * @param instance_id           instance id
	 * @param binding_id            binding id
	 * @param serviceBindingRequest request body
	 */
	public void validateGenerateServiceBindingRequest(String instance_id, String
			binding_id, ServiceBindingRequest serviceBindingRequest) {
		if (instanceService.getInstance(instance_id) == null) {
			throw new BadRequestException(ErrorCode.INSTANCE_NOT_FOUND, "The given instance_id does not exist");
		}
		if (binding_id == null || binding_id.isEmpty()) {
			throw new BadRequestException(ErrorCode.BINDING_MISSING, "The binding_id must be set");
		}
		if (serviceBindingRequest == null) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST, "No request body set");
		}
		if (!instanceService
				.getInstance(instance_id)
				.getService_id()
				.equals(serviceBindingRequest.getService_id())) {
			throw new BadRequestException(
					ErrorCode.INCOMPATIBLE_SERVICE,
					"The given service_id is not associated with the given instance_id"
			);
		}
		if (!instanceService.getInstance(instance_id).getPlan_id().equals(serviceBindingRequest.getPlan_id())) {
			throw new BadRequestException(
					ErrorCode.INCOMPATIBLE_PLAN,
					"The given plan_id is not associated with the given instance_id"
			);
		}
		String app_guid = serviceBindingRequest.getApp_guid();
		if (serviceBindingRequest.getBind_resource() != null && serviceBindingRequest
				                                                        .getBind_resource()
				                                                        .getApp_guid() != null) {
			if (app_guid != null && !Objects.equals(app_guid, serviceBindingRequest.getBind_resource().getApp_guid())) {
				throw new BadRequestException(ErrorCode.BAD_REQUEST, "Provided two different app_guid values");
			} else if (app_guid == null) {
				app_guid = serviceBindingRequest.getBind_resource().getApp_guid();
			}
		}
		if (app_guid == null) {
			throw new UnprocessableEntityException(
					ErrorCode.REQUIRES_APP,
					"This service broker requires the field app_guid"
			);
		}
		if (app_guid.isEmpty()) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST, "If set the app_guid must not be empty");
		}
		if (instanceService.getInstance(instance_id).getLastProvisionOperation() != null ||
		    instanceService.getInstance(instance_id).getLastUpdateOperation() != null ||
		    instanceService.getInstance(instance_id).getLastDeprovisionOperation() != null) {
			throw new UnprocessableEntityException(
					ErrorCode.CONCURRENCY_ERROR,
					"The service instance is currently blocked by another operation"
			);
		}
		if (bindingService.getBinding(instance_id, binding_id) != null && !bindingService.getBinding(
				instance_id,
				binding_id
		).equals(serviceBindingRequest)) {
			throw new ConflictException(
					ErrorCode.DUPLICATE_BINDING,
					"This binding id is already exists with different parameters"
			);
		}
	}

	/**
	 * validate the given request parameters for deleting a service binding
	 *
	 * @param instance_id instance id
	 * @param binding_id  binding id
	 * @param service_id  service id
	 * @param plan_id     plan id
	 */
	public void validateDeleteServiceBindingRequest(String instance_id, String binding_id, String
			service_id, String plan_id) {
		if (instanceService.getInstance(instance_id) == null) {
			throw new BadRequestException(ErrorCode.INSTANCE_NOT_FOUND, "The given instance_id does not exist");
		}
		if (binding_id == null || binding_id.isEmpty()) {
			throw new BadRequestException(ErrorCode.BINDING_MISSING, "The binding_id must be set");
		}
		if (bindingService.getBinding(instance_id, binding_id) == null) {
			throw new ElementGoneException(ErrorCode.BINDING_NOT_FOUND, "The given binding_id does not exist");
		}
		if (!instanceService.getInstance(instance_id).getService_id().equals(service_id)) {
			throw new BadRequestException(
					ErrorCode.INCOMPATIBLE_SERVICE,
					"The given service_id is not associated with the given instance_id"
			);
		}
		if (!instanceService.getInstance(instance_id).getPlan_id().equals(plan_id)) {
			throw new BadRequestException(
					ErrorCode.INCOMPATIBLE_PLAN,
					"The given plan_id is not associated with the given instance_id"
			);
		}
	}

	/**
	 * validate and check if the request was re-send
	 *
	 * @param instance_id instance id
	 * @param requestBody request body with instance information
	 */
	private void validateInstanceConflictFree(String instance_id, ProvisionServiceInstanceRequestBody
			requestBody) {
		Instance instance = instanceService.getInstance(instance_id);
		if (instance != null) {
			Instance requestedInstance = new Instance(
					requestBody.getService_id(),
					requestBody.getPlan_id(),
					null,
					requestBody.getContext(),
					requestBody.getOrganization_guid(),
					requestBody.getSpace_guid(),
					requestBody.getParameters(),
					new HashMap<>(),
					null,
					null,
					null,
					null
			);
			if (!checkForInstanceConflict(instance, requestedInstance)) {
				throw new ConflictException(
						ErrorCode.DUPLICATE_INSTANCE,
						"This instance_id is already used for the same service_id with different attributes"
				);
			}
		}
	}

	/**
	 * validate and check if the request was re-send
	 *
	 * @param instance_id instance id
	 * @param requestBody request body with instance information
	 *
	 * @return true if valid
	 */
	public boolean validateInstanceUpdateConflictFree(String instance_id, ServiceInstanceUpdateRequest
			requestBody) {
		Instance instance = instanceService.getInstance(instance_id);
		if (instance != null) {
			Instance requestedInstance = new Instance(
					requestBody.getService_id(),
					requestBody.getPlan_id(),
					null,
					requestBody.getContext(),
					instance.getOrganization_guid(),
					instance.getSpace_guid(),
					requestBody.getParameters(),
					new HashMap<>(),
					instance.getDashboard_url(),
					null,
					null,
					null
			);
			return checkForInstanceConflict(instance, requestedInstance);
		}
		return false;
	}

	/**
	 * Check if the platform use the correct version number of OSB-API
	 *
	 * @param apiVersion X-Broker-API-Version
	 */
	private void validateApiVersion(String apiVersion) {
		StringTokenizer stringTokenizer = new StringTokenizer(apiVersion, ".");
		int             majorVersion;
		int             minorVersion;
		try {
			majorVersion = Integer.parseInt(stringTokenizer.nextToken());
			minorVersion = Integer.parseInt(stringTokenizer.nextToken());
		} catch (NumberFormatException e) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST, "The API version is not in a proper format");
		}
		if (stringTokenizer.hasMoreTokens() || stringTokenizer.hasMoreElements()) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST, "The API version is not in a proper format");
		}
		if (majorVersion != 2 || minorVersion != 14) {
			throw new UnsupportedApiLevelException(
					ErrorCode.API_LEVEL_UNSUPPORTED,
					"This service broker only supports API Version 2.14"
			);
		}
	}

	/**
	 * check and validate given Orginating Identity
	 *
	 * @param originatingIdentity X-Broker-API-Originating-Identity
	 */
	private void validateOriginatingIdentity(String originatingIdentity) {
		if (originatingIdentity != null && !originatingIdentity.isEmpty()) {
			String[] strings = originatingIdentity.split(" ");
			if (strings.length != 2) {
				throw new BadRequestException(
						ErrorCode.ORIGINATING_IDENTITY_INVALID,
						"given X-Broker-API-Originating-Identity is not in a correct format: 'platform value'"
				);
			}
			String decodedValue = new String(Base64.getDecoder().decode(strings[1]), StandardCharsets.UTF_8);
			//String encodedValue = Base64.getEncoder().encodeToString(strings[1].getBytes(StandardCharsets.UTF_8));
			try {
				HashMap<String, Object> value = objectMapper.readValue(
						decodedValue,
						new TypeReference<HashMap<String, Object>>() {
						}
				);
				switch (strings[0]) {
					case ("cloudfoundry"):
						if (!value.containsKey("user_id")) {
							throw new BadRequestException(
									ErrorCode.ORIGINATING_IDENTITY_INVALID,
									"missing user_id for platform cloudfoundry in value"
							);
						}
						break;
					case ("kubernetes"):
						if (!value.containsKey("username") || !value.containsKey("uid") || !value.containsKey("groups") || !value
								.containsKey("extra")) {
							throw new BadRequestException(
									ErrorCode.ORIGINATING_IDENTITY_INVALID,
									"missing information for platform kubernetes in value"
							);
						}
						break;
					default:
						throw new BadRequestException(
								ErrorCode.ORIGINATING_IDENTITY_INVALID,
								String.format("not supported platform %s given", strings[0])
						);
				}
			} catch (IOException e) {
				throw new BadRequestException(
						ErrorCode.ORIGINATING_IDENTITY_INVALID,
						"cant deserialize the given value of X-Broker-API-Originating-Identity"
				);
			}
		}

	}

	/**
	 * Check if asynchronous responses are supported by the platform
	 *
	 * @param asyncSupported sent support asynchronous responses supported
	 */
	private void validateRequiredAsyncSupport(boolean asyncSupported) {
		if (!asyncSupported) {
			throw new UnprocessableEntityException(
					ErrorCode.ASYNC_REQUIRED,
					"This Service Plan requires client support for asynchronous service operations."
			);
		}
	}

	/**
	 * check if the given request was re-send
	 *
	 * @param existingInstance  existing instance
	 * @param requestedInstance request filled in an instance object
	 *
	 * @return true: instance-information equal, false: instance-information not equal
	 */
	private boolean checkForInstanceConflict(Instance existingInstance, Instance requestedInstance) {
		return requestedInstance.getService_id().equals(existingInstance.getService_id()) &&
		       requestedInstance.getPlan_id().equals(existingInstance.getPlan_id()) &&
		       (requestedInstance.getContext() == null || (requestedInstance.getContext() != null && requestedInstance
				       .getContext()
				       .equals(existingInstance.getContext()))) &&
		       requestedInstance.getOrganization_guid().equals(existingInstance.getOrganization_guid()) &&
		       requestedInstance.getSpace_guid().equals(existingInstance.getSpace_guid()) &&
		       (requestedInstance.getRequestedParameters() == null || (requestedInstance.getRequestedParameters() != null && requestedInstance
				       .getRequestedParameters()
				       .equals(existingInstance.getRequestedParameters())));
	}

	/**
	 * check if there is an active operation on the given instance
	 *
	 * @param instance_id instance id
	 * @param operation   given operation of actual request
	 *
	 * @return true: existing different active operation; false: no different existing operation
	 */
	private boolean checkForDifferentActiveOperation(String instance_id, Operation operation) {
		Instance instance = instanceService.getInstance(instance_id);
		if (instance != null) {
			switch (operation) {
				case PROVISION:
					if (instance.getLastUpdateOperation() != null) {
						return true;
					}
					if (instance.getLastDeprovisionOperation() != null) {
						return true;
					}
					break;
				case UPDATE:
					if (instance.getLastProvisionOperation() != null) {
						return true;
					}
					if (instance.getLastDeprovisionOperation() != null) {
						return true;
					}
					break;
				case DEPROVISION:
					if (instance.getLastProvisionOperation() != null) {
						return true;
					}
					if (instance.getLastUpdateOperation() != null) {
						return true;
					}
					break;
			}
		}
		return false;
	}
}
