package com.lanteam.kubernetesapi.server.controller;

import com.lanteam.kubernetesapi.server.exceptions.BadRequestException;
import com.lanteam.kubernetesapi.server.model.EmptyResponse;
import com.lanteam.kubernetesapi.server.model.ErrorCode;
import com.lanteam.kubernetesapi.server.model.OperationState;
import com.lanteam.kubernetesapi.server.model.request.GlobalRequestHeader;
import com.lanteam.kubernetesapi.server.model.request.ServiceBindingRequest;
import com.lanteam.kubernetesapi.server.model.response.Error;
import com.lanteam.kubernetesapi.server.model.response.*;
import com.lanteam.kubernetesapi.server.service.BindingService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * The Service binding controller
 */
@Slf4j
@RestController
@Api(value = "", description = " ", tags = "3. ServiceBindings")
public class ServiceBindingsController {

	/**
	 * RestRequestValidator: set of validation functions for the request
	 */
	private RestRequestValidator restRequestValidator;
	/**
	 * BindingService: set of binding functions
	 */
	private BindingService       bindingService;

	/**
	 * Constructor
	 *
	 * @param restRequestValidator RestRequestValidator
	 * @param bindingService       BindingService
	 */
	@Autowired
	ServiceBindingsController(RestRequestValidator restRequestValidator, BindingService bindingService) {
		this.restRequestValidator = restRequestValidator;
		this.bindingService = bindingService;
	}

	/**
	 * generation of a service binding
	 *
	 * @param apiVersion          X-Broker-API-Version
	 * @param originatingIdentity X-Broker-API-Originating-Identity
	 * @param instance_id         instance id of instance to create a binding on
	 * @param binding_id          binding id of binding to create
	 * @param acceptIncomplete    asynchronous operations supported
	 * @param requestBody         ServiceBindingRequest
	 *
	 * @return Response, if generation was ok, created or accepted
	 */
	@PutMapping("/v2/service_instances/{instance_id}/service_bindings/{binding_id}")
	@ApiOperation(
			value = "generation of a service binding",
			response = ServiceBinding.class,
			authorizations = {@Authorization(value = "basicAuth")}
	)
	@ApiImplicitParams(value = {
			@ApiImplicitParam(
					name = "X-Broker-API-Version",
					required = true,
					defaultValue = "2.14",
					value = "version number of the Service Broker API that the Platform will use",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "X-Broker-API-Originating-Identity",
					value = "identity of the user that initiated the request from the Platform",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "instance_id",
					required = true,
					value = "instance id of instance to create a binding on",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "binding_id",
					required = true,
					value = "binding id of binding to create",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "accepts_incomplete",
					value = "asynchronous operations supported",
					dataType = "boolean"
			),
	})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = ServiceBinding.class),
			@ApiResponse(code = 201, message = "Created", response = ServiceBinding.class),
			@ApiResponse(code = 400, message = "Bad Request", response = Error.class),
			@ApiResponse(code = 409, message = "Conflict", response = Error.class),
			@ApiResponse(code = 422, message = "Unprocessable Entity", response = Error.class)
	})
	ResponseEntity<ServiceBinding> generateServiceBinding(
			@RequestHeader("X-Broker-API-Version") String apiVersion,
			@RequestHeader(value = "X-Broker-API-Originating-Identity", required = false) String originatingIdentity,
			@PathVariable String instance_id,
			@PathVariable String binding_id,
			@RequestParam(name = "accepts_incomplete", required = false) boolean acceptIncomplete,
			@RequestBody ServiceBindingRequest requestBody) {
		GlobalRequestHeader requestHeader = new GlobalRequestHeader(apiVersion, originatingIdentity);
		this.restRequestValidator.validateHeader(requestHeader);
		restRequestValidator.validateGenerateServiceBindingRequest(instance_id, binding_id, requestBody);

		// If service binding already exists (with same requestBody information) return already created service binding
		if (bindingService.getBinding(instance_id, binding_id) != null) {
			return new ResponseEntity<>(new ServiceBinding(bindingService.getCredentials(instance_id), null, null, null), HttpStatus.OK );
		}

		return new ResponseEntity<>(
				bindingService.createBinding(instance_id, binding_id, requestBody),
				HttpStatus.CREATED
		);
	}

	/**
	 * deletion of a service binding
	 *
	 * @param apiVersion          X-Broker-API-Version
	 * @param originatingIdentity X-Broker-API-Originating-Identity
	 * @param instance_id         id of the instance associated with the binding being deleted
	 * @param binding_id          id of the binding being deleted
	 * @param service_id          id of the service associated with the instance for which a binding is being deleted
	 * @param plan_id             id of the plan associated with the instance for which a binding is being deleted
	 * @param acceptIncomplete    asynchronous operations supported
	 *
	 * @return response, if deletion is ok or accepted
	 */
	@DeleteMapping("/v2/service_instances/{instance_id}/service_bindings/{binding_id}")
	@ApiOperation(
			value = "deletion of a service binding",
			response = Object.class,
			authorizations = {@Authorization(value = "basicAuth")}
	)
	@ApiImplicitParams(value = {
			@ApiImplicitParam(
					name = "X-Broker-API-Version",
					required = true,
					defaultValue = "2.14",
					value = "version number of the Service Broker API that the Platform will use",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "X-Broker-API-Originating-Identity",
					value = "identity of the user that initiated the request from the Platform",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "instance_id",
					required = true,
					value = "id of the instance associated with the binding being deleted",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "binding_id",
					required = true,
					value = "id of the binding being deleted",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "service_id",
					required = true,
					value = "id of the service associated with the instance for which a binding is being deleted",
					dataType = "string"
			),
			@ApiImplicitParam(
					name = "plan_id",
					required = true,
					value = "id of the plan associated with the instance for which a binding is being deleted",
					dataType = "string"
			),
			@ApiImplicitParam(
					name = "accepts_incomplete",
					value = "asynchronous operations supported",
					dataType = "boolean"
			)
	})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = EmptyResponse.class),
			@ApiResponse(code = 400, message = "Bad Request", response = Error.class),
			@ApiResponse(code = 410, message = "Gone", response = Error.class)
	})
	ResponseEntity<EmptyResponse> deleteServiceBinding(
			@RequestHeader("X-Broker-API-Version") String apiVersion,
			@RequestHeader(value = "X-Broker-API-Originating-Identity", required = false) String originatingIdentity,
			@PathVariable String instance_id,
			@PathVariable String binding_id,
			@RequestParam String service_id,
			@RequestParam String plan_id,
			@RequestParam(name = "accepts_incomplete", required = false) boolean acceptIncomplete) {
		GlobalRequestHeader requestHeader = new GlobalRequestHeader(apiVersion, originatingIdentity);
		this.restRequestValidator.validateHeader(requestHeader);
		restRequestValidator.validateDeleteServiceBindingRequest(instance_id, binding_id, service_id, plan_id);

		bindingService.deleteBinding(instance_id, binding_id);
		return new ResponseEntity<>(new EmptyResponse(), HttpStatus.OK);
	}

	/**
	 * gets a service binding
	 *
	 * @param apiVersion          X-Broker-API-Version
	 * @param originatingIdentity X-Broker-API-Originating-Identity
	 * @param instance_id         id of the instance associated with the binding being deleted
	 * @param binding_id          id of the binding being deleted
	 *
	 * @return response service binding information
	 */
	@GetMapping("/v2/service_instances/{instance_id}/service_bindings/{binding_id}")
	@ApiOperation(
			value = "gets a service binding",
			response = ServiceBindingResource.class,
			authorizations = {@Authorization(value = "basicAuth")}
	)
	@ApiImplicitParams(value = {
			@ApiImplicitParam(
					name = "X-Broker-API-Version",
					required = true,
					defaultValue = "2.14",
					value = "version number of the Service Broker API that the Platform will use",
					type = "String"
			),
			@ApiImplicitParam(
					name = "X-Broker-API-Originating-Identity",
					value = "identity of the user that initiated the request from the Platform",
					type = "String"
			),
			@ApiImplicitParam(
					name = "instance_id",
					required = true,
					value = "id of the instance associated with the binding being deleted",
					type = "String"
			),
			@ApiImplicitParam(
					name = "binding_id",
					required = true,
					value = "id of the binding being deleted",
					type = "String"
			)
	})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = ServiceBindingResource.class),
			@ApiResponse(code = 404, message = "Not Found", response = Error.class)
	})
	ResponseEntity<ServiceBindingResource> getServiceBinding(
			@RequestHeader("X-Broker-API-Version") String apiVersion,
			@RequestHeader(value = "X-Broker-API-Originating-Identity", required = false) String originatingIdentity,
			@PathVariable String instance_id,
			@PathVariable String binding_id) {
		GlobalRequestHeader requestHeader = new GlobalRequestHeader(apiVersion, originatingIdentity);
		this.restRequestValidator.validateHeader(requestHeader);

		ServiceBindingResource resource = new ServiceBindingResource();
		ServiceBindingRequest request = bindingService.getBinding(instance_id, binding_id);
		resource.setParameters(request.getParameters());
		resource.setCredentials(bindingService.getCredentials(instance_id));

		return new ResponseEntity<>(resource, HttpStatus.OK);
	}

	/**
	 * last requested operation state for service binding
	 *
	 * @param apiVersion  X-Broker-API-Version
	 * @param instance_id instance id of instance to find last operation applied to it
	 * @param binding_id  binding id of service binding to find last operation applied to it
	 * @param service_id  id of the service associated with the instance
	 * @param plan_id     id of the plan associated with the instance
	 * @param operation   a provided identifier for the operation
	 *
	 * @return LastOperation
	 */
	@GetMapping("/v2/service_instances/{instance_id}/service_bindings/{binding_id}/last_operation")
	@ApiOperation(
			value = "last requested operation state for service binding",
			response = LastOperation.class,
			authorizations = {@Authorization(value = "basicAuth")}
	)
	@ApiImplicitParams(value = {
			@ApiImplicitParam(
					name = "X-Broker-API-Version",
					required = true,
					defaultValue = "2.14",
					value = "version number of the Service Broker API that the Platform will use",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "instance_id",
					required = true,
					value = "instance id of instance to find last operation applied to it",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "binding_id",
					required = true,
					value = "binding id of service binding to find last operation applied to it",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "service_id",
					value = "id of the service associated with the instance",
					dataType = "string"
			),
			@ApiImplicitParam(
					name = "plan_id",
					value = "id of the plan associated with the instance",
					dataType = "string"
			),
			@ApiImplicitParam(
					name = "operation",
					value = "a provided identifier for the operation",
					dataType = "string"
			)
	})
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Bad Request", response = Error.class)
	})
	LastOperation getLastOperationOfServiceBinding(
			@RequestHeader("X-Broker-API-Version") String apiVersion,
			@PathVariable String instance_id,
			@PathVariable String binding_id,
			@RequestParam(required = false) String service_id,
			@RequestParam(required = false) String plan_id,
			@RequestParam(required = false) String operation) {
		GlobalRequestHeader requestHeader = new GlobalRequestHeader(apiVersion);
		throw new BadRequestException(ErrorCode.BAD_REQUEST, "This endpoint is not in use, everything is synchronous");
	}

}
