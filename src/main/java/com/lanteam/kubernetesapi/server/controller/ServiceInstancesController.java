package com.lanteam.kubernetesapi.server.controller;

import com.lanteam.kubernetesapi.server.model.Instance;
import com.lanteam.kubernetesapi.server.model.request.GlobalRequestHeader;
import com.lanteam.kubernetesapi.server.model.request.ProvisionServiceInstanceRequestBody;
import com.lanteam.kubernetesapi.server.model.request.ServiceInstanceUpdateRequest;
import com.lanteam.kubernetesapi.server.model.response.Error;
import com.lanteam.kubernetesapi.server.model.response.LastOperation;
import com.lanteam.kubernetesapi.server.model.response.ServiceInstanceResource;
import com.lanteam.kubernetesapi.server.model.response.ServiceInstanceResponse;
import com.lanteam.kubernetesapi.server.service.InstanceService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * the Service instance controller
 */
@Slf4j
@RestController
@Api(value = "", description = " ", tags = "2. ServiceInstances")
public class ServiceInstancesController {
	/**
	 * RestRequestValidator: set of validation functions for the request
	 */
	private RestRequestValidator restRequestValidator;
	/**
	 * InstanceService: set of instance functions
	 */
	private InstanceService      instanceService;

	/**
	 * Constructor
	 *
	 * @param restRequestValidator RestRequestValidator
	 * @param instanceService      InstanceService
	 */
	@Autowired
	ServiceInstancesController(RestRequestValidator restRequestValidator, InstanceService instanceService) {
		this.restRequestValidator = restRequestValidator;
		this.instanceService = instanceService;
	}

	/**
	 * provision a service instance
	 *
	 * @param apiVersion          X-Broker-API-Version
	 * @param originatingIdentity X-Broker-API-Originating-Identity
	 * @param instance_id         instance id of instance to provision
	 * @param acceptIncomplete    asynchronous operations supported
	 * @param requestBody         ProvisionServiceInstanceRequestBody
	 *
	 * @return Response, if provision was ok, created or accepted
	 */
	@PutMapping("/v2/service_instances/{instance_id}")
	@ApiOperation(
			value = "provision a service instance",
			response = ServiceInstanceResponse.class,
			authorizations = {@Authorization(value = "basicAuth")}
	)
	@ApiImplicitParams(value = {
			@ApiImplicitParam(
					name = "X-Broker-API-Version",
					required = true,
					defaultValue = "2.14",
					value = "version number of the Service Broker API that the Platform will use",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "X-Broker-API-Originating-Identity",
					value = "identity of the user that initiated the request from the Platform",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "instance_id",
					required = true,
					value = "instance id of instance to provision",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "accepts_incomplete",
					value = "asynchronous operations supported",
					dataType = "boolean"
			)
	})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = ServiceInstanceResponse.class),
			@ApiResponse(code = 202, message = "Accepted", response = ServiceInstanceResponse.class),
			@ApiResponse(code = 400, message = "Bad Request", response = Error.class),
			@ApiResponse(code = 409, message = "Conflict", response = Error.class),
			@ApiResponse(code = 422, message = "Unprocessable Entity", response = Error.class)
	})
	ResponseEntity<ServiceInstanceResponse> provisionServicesInstance(
			@RequestHeader("X-Broker-API-Version") String apiVersion,
			@RequestHeader(value = "X-Broker-API-Originating-Identity", required = false) String originatingIdentity,
			@PathVariable String instance_id,
			@RequestParam(name = "accepts_incomplete", required = false) boolean acceptIncomplete,
			@RequestBody ProvisionServiceInstanceRequestBody requestBody) {
		GlobalRequestHeader requestHeader = new GlobalRequestHeader(apiVersion, originatingIdentity);
		restRequestValidator.validateHeader(requestHeader);
		restRequestValidator.validateProvisionServiceInstanceRequest(instance_id, requestBody, acceptIncomplete);

		Instance instance = instanceService.provisionInstance(instance_id, requestBody);
		ServiceInstanceResponse response = new ServiceInstanceResponse(
				instance.getDashboard_url(),
				instance.getLastProvisionOperation()
		);
		return new ResponseEntity<>(response, instance.getLastProvisionOperation() == null ? HttpStatus.OK : HttpStatus.ACCEPTED);
	}

	/**
	 * update a service instance
	 *
	 * @param apiVersion          X-Broker-API-Version
	 * @param originatingIdentity X-Broker-API-Originating-Identity
	 * @param instance_id         instance id of instance to update
	 * @param acceptIncomplete    synchronous operations supported
	 * @param requestBody         ServiceInstanceUpdateRequest
	 *
	 * @return Response, if update was ok or accepted
	 */
	@PatchMapping("/v2/service_instances/{instance_id}")
	@ApiOperation(
			value = "update a service instance",
			response = ServiceInstanceResponse.class,
			authorizations = {@Authorization(value = "basicAuth")}
	)
	@ApiImplicitParams(value = {
			@ApiImplicitParam(
					name = "X-Broker-API-Version",
					required = true,
					defaultValue = "2.14", value = "version number of the Service Broker API that the Platform will use",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "X-Broker-API-Originating-Identity",
					value = "identity of the user that initiated the request from the Platform",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "instance_id", required = true,
					value = "instance id of instance to update",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "accepts_incomplete",
					value = "asynchronous operations supported",
					dataType = "boolean"
			)
	})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = ServiceInstanceResponse.class),
			@ApiResponse(code = 202, message = "Accepted", response = ServiceInstanceResponse.class),
			@ApiResponse(code = 400, message = "Bad Request", response = Error.class),
			@ApiResponse(code = 422, message = "Unprocessable Entity", response = Error.class)
	})
	ResponseEntity<ServiceInstanceResponse> updateServicesInstance(
			@RequestHeader("X-Broker-API-Version") String apiVersion,
			@RequestHeader(value = "X-Broker-API-Originating-Identity", required = false) String originatingIdentity,
			@PathVariable String instance_id,
			@RequestParam(name = "accepts_incomplete", required = false) boolean acceptIncomplete,
			@RequestBody ServiceInstanceUpdateRequest requestBody) {
		GlobalRequestHeader requestHeader = new GlobalRequestHeader(apiVersion, originatingIdentity);
		restRequestValidator.validateHeader(requestHeader);
		restRequestValidator.validateUpdateServiceInstanceRequest(instance_id, requestBody, acceptIncomplete);

		boolean sameData = restRequestValidator.validateInstanceUpdateConflictFree(instance_id, requestBody);
		if (sameData && instanceService.getInstance(instance_id).getLastUpdateOperation() == null) {
			return new ResponseEntity<>(new ServiceInstanceResponse(), HttpStatus.OK);
		}

		Instance instance = instanceService.updateInstance(instance_id, requestBody);
		return new ResponseEntity<>(new ServiceInstanceResponse(
				instance.getDashboard_url(),
				instance.getLastUpdateOperation()
		), HttpStatus.ACCEPTED);
	}

	/**
	 * deprovision a service instance
	 *
	 * @param apiVersion       X-Broker-API-Version
	 * @param instance_id      id of instance being deleted
	 * @param service_id       id of the service associated with the instance being deleted
	 * @param plan_id          id of the plan associated with the instance being deleted
	 * @param acceptIncomplete asynchronous operations supported
	 *
	 * @return Response, if delete was ok or accepted
	 */
	@DeleteMapping("/v2/service_instances/{instance_id}")
	@ApiOperation(
			value = "deprovision a service instance",
			response = ServiceInstanceResponse.class,
			authorizations = {@Authorization(value = "basicAuth")}
	)
	@ApiImplicitParams(value = {
			@ApiImplicitParam(
					name = "X-Broker-API-Version",
					required = true,
					defaultValue = "2.14",
					value = "version number of the Service Broker API that the Platform will use",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "instance_id",
					required = true,
					value = "id of instance being deleted",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "service_id",
					required = true,
					value = "id of the service associated with the instance being deleted",
					dataType = "string"
			),
			@ApiImplicitParam(
					name = "plan_id",
					required = true,
					value = "id of the plan associated with the instance being deleted",
					dataType = "string"
			),
			@ApiImplicitParam(
					name = "accepts_incomplete",
					value = "asynchronous operations supported",
					dataType = "boolean"
			)
	})
	@ApiResponses(value = {
			@ApiResponse(code = 202, message = "Accepted", response = ServiceInstanceResponse.class),
			@ApiResponse(code = 400, message = "Bad Request", response = Error.class),
			@ApiResponse(code = 410, message = "Gone", response = Error.class),
			@ApiResponse(code = 422, message = "Unprocessable Entity", response = Error.class)
	})
	ResponseEntity<ServiceInstanceResponse> deleteServicesInstance(
			@RequestHeader("X-Broker-API-Version") String apiVersion,
			@PathVariable String instance_id,
			@RequestParam(name = "service_id") String service_id,
			@RequestParam(name = "plan_id") String plan_id,
			@RequestParam(name = "accepts_incomplete", required = false) boolean acceptIncomplete) {

		GlobalRequestHeader requestHeader = new GlobalRequestHeader(apiVersion);
		restRequestValidator.validateHeader(requestHeader);
		restRequestValidator.validateDeleteServiceInstanceRequest(instance_id, service_id, plan_id, acceptIncomplete);

		Instance instance = instanceService.deprovisionInstance(instance_id);

		ServiceInstanceResponse response = new ServiceInstanceResponse();
		response.setOperation(instance.getLastDeprovisionOperation());

		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}

	/**
	 * gets a service instance
	 *
	 * @param apiVersion          X-Broker-API-Version
	 * @param originatingIdentity X-Broker-API-Originating-Identity
	 * @param instance_id         instance id of instance to fetch
	 *
	 * @return Response service instance information
	 */
	@GetMapping("/v2/service_instances/{instance_id}")
	@ApiOperation(
			value = "gets a service instance",
			response = ServiceInstanceResource.class,
			authorizations = {@Authorization(value = "basicAuth")}
	)
	@ApiImplicitParams(value = {
			@ApiImplicitParam(
					name = "X-Broker-API-Version",
					required = true, defaultValue = "2.14",
					value = "version number of the Service Broker API that the Platform will use",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "X-Broker-API-Originating-Identity",
					value = "identity of the user that initiated the request from the Platform",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "instance_id", required = true,
					value = "instance id of instance to fetch",
					dataType = "String"
			)
	})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = ServiceInstanceResource.class),
			@ApiResponse(code = 404, message = "Not Found", response = Error.class),
			@ApiResponse(code = 422, message = "Unprocessable Entity", response = Error.class)
	})
	ResponseEntity<ServiceInstanceResource> getServicesInstance(
			@RequestHeader("X-Broker-API-Version") String apiVersion,
			@RequestHeader(value = "X-Broker-API-Originating-Identity", required = false) String originatingIdentity,
			@PathVariable String instance_id) {
		GlobalRequestHeader requestHeader = new GlobalRequestHeader(apiVersion, originatingIdentity);
		restRequestValidator.validateHeader(requestHeader);
		restRequestValidator.validateGetServiceInstanceRequest(instance_id);

		return new ResponseEntity<>(instanceService.getInstanceResource(instance_id), HttpStatus.OK);
	}

	/**
	 * last requested operation state for service instance
	 *
	 * @param apiVersion  X-Broker-API-Version
	 * @param instance_id instance id of instance to find last operation applied to it
	 * @param service_id  id of the service associated with the instance
	 * @param plan_id     id of the plan associated with the instance
	 * @param operation   a provided identifier for the operation
	 *
	 * @return response status of last operation on a service instance
	 */
	@GetMapping("/v2/service_instances/{instance_id}/last_operation")
	@ApiOperation(
			value = "last requested operation state for service instance",
			response = LastOperation.class,
			authorizations = {@Authorization(value = "basicAuth")}
	)
	@ApiImplicitParams(value = {
			@ApiImplicitParam(
					name = "X-Broker-API-Version",
					required = true,
					defaultValue = "2.14",
					value = "version number of the Service Broker API that the Platform will use",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "instance_id",
					required = true,
					value = "instance id of instance to find last operation applied to it",
					dataType = "String"
			),
			@ApiImplicitParam(
					name = "service_id",
					value = "id of the service associated with the instance",
					dataType = "string"
			),
			@ApiImplicitParam(
					name = "plan_id",
					value = "id of the plan associated with the instance",
					dataType = "string"
			),
			@ApiImplicitParam(
					name = "operation",
					value = "a provided identifier for the operation",
					dataType = "string"
			)
	})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = LastOperation.class),
			@ApiResponse(code = 400, message = "Bad Request", response = Error.class),
			@ApiResponse(code = 410, message = "Gone", response = Error.class)
	})
	ResponseEntity<LastOperation> getLastOperationOfServicesInstance(
			@RequestHeader("X-Broker-API-Version") String apiVersion,
			@PathVariable String instance_id,
			@RequestParam(required = false) String service_id,
			@RequestParam(required = false) String plan_id,
			@RequestParam(required = false) String operation) {
		GlobalRequestHeader requestHeader = new GlobalRequestHeader(apiVersion);
		restRequestValidator.validateHeader(requestHeader);
		restRequestValidator.validateGetLastOperationOfServiceInstanceRequest(
				instance_id,
				service_id,
				plan_id,
				operation
		);

		LastOperation lastOperation = instanceService.lastOperationInstance(instance_id, operation);
		return new ResponseEntity<>(lastOperation != null ? lastOperation : new LastOperation(), HttpStatus.OK);
	}

}
