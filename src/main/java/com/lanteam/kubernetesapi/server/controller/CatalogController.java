package com.lanteam.kubernetesapi.server.controller;

import com.lanteam.kubernetesapi.server.model.request.GlobalRequestHeader;
import com.lanteam.kubernetesapi.server.model.response.Catalog;
import com.lanteam.kubernetesapi.server.service.CatalogService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 * Catalog controller
 */
@Slf4j
@RestController
@Api(value = "", description = " ", tags = "1. Catalog")
public class CatalogController {

	/**
	 * RestRequestValidator: set of validation functions for the request
	 */
	private RestRequestValidator restRequestValidator;
	/**
	 * CatalogService: Get a defined catalog with services
	 */
	private CatalogService       catalogService;

	/**
	 * Constructor
	 *
	 * @param restRequestValidator restRequestValidator
	 * @param catalogService       catalogService
	 */
	@Autowired
	CatalogController(RestRequestValidator restRequestValidator, CatalogService catalogService) {
		this.restRequestValidator = restRequestValidator;
		this.catalogService = catalogService;
	}

	/**
	 * Get-Function, to show platform the offered services with plans
	 *
	 * @param apiVersion X-Broker-API-Version
	 *
	 * @return Catalog of given services
	 */
	@GetMapping("/v2/catalog")
	@ApiOperation(
			value = "get the catalog of services that the service broker offers",
			response = Catalog.class,
			authorizations = {
					@Authorization(value = "basicAuth")
			}
	)
	@ApiImplicitParams(value = {
			@ApiImplicitParam(
					name = "X-Broker-API-Version",
					required = true,
					defaultValue = "2.14",
					value = "version number of the Service Broker API that the Platform will use",
					type = "String"
			)
	})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = Catalog.class),
	})
	Catalog getCatalog(@RequestHeader("X-Broker-API-Version") String apiVersion) {
		GlobalRequestHeader requestHeader = new GlobalRequestHeader(apiVersion);
		this.restRequestValidator.validateHeader(requestHeader);
		return new Catalog(catalogService.getServices());
	}
}
