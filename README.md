# Lanteam - OSB Redis Service
This project contains the implementation of our OSB Redis Service.

# Usage
Some commands for usage purpose:
## test

```bash
./mvnw test
```

## javadoc

```bash
./mvnw javadoc:javadoc
```
open `target/site/apidocs/index.html` for the documentation.

## pack jar

```bash
./mvnw clean install spring-boot:repackage
```
You will find it under `target/` and start it via `java -jar server-1.2.1-STABLE.jar`

## Helm 

You can install our helm package by using the commands below:

```bash
cd ./helm/charts
helm install ./redis --set Variation=small
```

## Cleanup
Clean up your minikube workspace
```bash
helm ls --all --short | xargs -L1 helm delete --purge; kubectl delete pvc --all; kubectl delete pv --all
```

# osb-checker-kotlin
## Config
*application.yml*
```yaml
config:
    url: http://localhost
    port: 8080
    apiVersion: 2.14
    user: lanteam
    password: eproisttoll
```
Start with `-cat -prov -con -auth -bind`

## Important information
We suppose that the java helm lib `microbean` has some major bugs. 
We use this lib for (un-)deployment of our helm packages. On different plans on different 
machines the delete method doesn't trigger as expected and it needs several minutes to
undeploy a package. This means that the  `DELETE` request to an instance can take up to
10min so sit back, `fetch(coffee)` and relax. 
## Result
```
╷
└─ JUnit Jupiter ✔
   ├─ CatalogJUnit5 ✔
   │  └─ validateCatalog() ✔
   ├─ ProvisionJUnit5 ✔
   │  ├─ runSyncTest() ✔
   │  │  └─ should handle sync requests correctly ✔
   │  │     ├─ Sync PUT request ✔
   │  │     └─ Sync DELETE request ✔
   │  ├─ runInvalidAsyncPutTest() ✔
   │  │  ├─ PUT should reject if missing service_id ✔
   │  │  ├─ PUT should reject if missing plan_id ✔
   │  │  ├─ PUT should reject if missing service_id field ✔
   │  │  ├─ PUT should reject if missing plan_id field ✔
   │  │  ├─ PUT should reject if missing service_id field ✔
   │  │  ├─ PUT should reject if missing service_id field ✔
   │  │  ├─ PUT should reject if missing service_id is Invalid ✔
   │  │  └─ PUT should reject if missing plan_id is Invalid ✔
   │  └─ runInvalidAsyncDeleteTest() ✔
   │     ├─ DELETE should reject if service_id is missing ✔
   │     └─ DELETE should reject if plan_id is missing ✔
   ├─ BindingJUnit5 ✔
   │  ├─ runInvalidBindingAttempts() ✔
   │  │  ├─ Provision and in case of a async service broker polling, for later binding ✔
   │  │  │  └─ Running valid PUT provision with instanceId 4e1e2de9-23ea-470d-be2b-984952adad72 for service ca95b9b0-2590-4319-b178-20b5217cd124 and plan small id: 9793eddb-6308-4e45-9bee-03f891da6ccf 21482 ms ✔
   │  │  ├─ Running invalid bindings ✔
   │  │  │  ├─ PUT should reject if missing service_id ✔
   │  │  │  ├─ DELETE should reject if missing service_id ✔
   │  │  │  ├─ PUT should reject if missing plan_id ✔
   │  │  │  └─ DELETE should reject if missing plan_id ✔
   │  │  └─ Deleting provision ✔
   │  │     └─ DELETE provision and if the service broker is async polling afterwards 10322 ms ✔
   │  └─ runValidBindings() ✔
   │     ├─ Running a valid provision if the service is bindable a valid binding. Deleting both afterwards. In case of a async service broker poll afterwards. ✔
   │     │  ├─ Provision and in case of a async service broker polling, for later binding ✔
   │     │  │  └─ Running valid PUT provision with instanceId 806a502b-8e37-4a94-ac1d-f0c49c6ce1e3 for service ca95b9b0-2590-4319-b178-20b5217cd124 and plan small id: 9793eddb-6308-4e45-9bee-03f891da6ccf 20976 ms ✔
   │     │  ├─ Running PUT binding and DELETE binding afterwards ✔
   │     │  │  ├─ Running a valid binding with bindingId e2df0837-2162-47a9-b751-b9f13890f500 ✔
   │     │  │  └─ Deleting binding with bindingId e2df0837-2162-47a9-b751-b9f13890f500 ✔
   │     │  └─ Deleting provision ✔
   │     │     └─ DELETE provision and if the service broker is async polling afterwards 10298 ms ✔
   │     ├─ Running a valid provision if the service is bindable a valid binding. Deleting both afterwards. In case of a async service broker poll afterwards. ✔
   │     │  ├─ Provision and in case of a async service broker polling, for later binding ✔
   │     │  │  └─ Running valid PUT provision with instanceId 4653e361-bb19-437a-8e47-f554a0495527 for service ca95b9b0-2590-4319-b178-20b5217cd124 and plan standard id: 7fe8f734-fad5-4774-b94b-5e84fa8a838a 10732 ms ✔
   │     │  ├─ Running PUT binding and DELETE binding afterwards ✔
   │     │  │  ├─ Running a valid binding with bindingId 67d01db8-f105-46d4-9dc3-1163409c3e5c ✔
   │     │  │  └─ Deleting binding with bindingId 67d01db8-f105-46d4-9dc3-1163409c3e5c ✔
   │     │  └─ Deleting provision ✔
   │     │     └─ DELETE provision and if the service broker is async polling afterwards 596292 ms ✔
   │     └─ Running a valid provision if the service is bindable a valid binding. Deleting both afterwards. In case of a async service broker poll afterwards. ✔
   │        ├─ Provision and in case of a async service broker polling, for later binding ✔
   │        │  └─ Running valid PUT provision with instanceId 331a19e3-d78d-41cc-884f-8c8cc2046c59 for service ca95b9b0-2590-4319-b178-20b5217cd124 and plan cluster id: e7a35ed7-a02e-443c-993a-1cf8536d37a3 81471 ms ✔
   │        ├─ Running PUT binding and DELETE binding afterwards ✔
   │        │  ├─ Running a valid binding with bindingId c0de5474-b739-41d7-bec1-5a65f7f3c8ae ✔
   │        │  └─ Deleting binding with bindingId c0de5474-b739-41d7-bec1-5a65f7f3c8ae ✔
   │        └─ Deleting provision ✔
   │           └─ DELETE provision and if the service broker is async polling afterwards 40577 ms ✔
   ├─ AuthenticationJUnit5 ✔
   │  └─ testAuthentication() ✔
   │     └─ Requests without authentication should be rejected ✔
   │        ├─ GET - v2/catalog should reject with 401 ✔
   │        │  ├─ Without authentication ✔
   │        │  ├─ With wrong Username ✔
   │        │  └─ With wrong Password ✔
   │        ├─ PUT - v2/service_instance/instance_id should reject with 401 ✔
   │        │  ├─ Without authentication ✔
   │        │  ├─ With wrong Username ✔
   │        │  └─ With wrong Password ✔
   │        ├─ DELETE - v2/service_instance/instance_id should reject with 401 ✔
   │        │  ├─ Without authentication ✔
   │        │  ├─ With wrong Username ✔
   │        │  └─ With wrong Password ✔
   │        ├─ GET - v2/service_instance/instance_id/last_operation should reject with 401 ✔
   │        │  ├─ Without authentication ✔
   │        │  ├─ With wrong Username ✔
   │        │  └─ With wrong Password ✔
   │        ├─ PUT - v2/service_instance/instance_id/service_binding/binding_id  should reject with 401 ✔
   │        │  ├─ Without authentication ✔
   │        │  ├─ With wrong Username ✔
   │        │  └─ With wrong Password ✔
   │        └─ DELETE - v2/service_instance/instance_id/service_binding/binding_id  should reject with 401 ✔
   │           ├─ Without authentication ✔
   │           ├─ With wrong Username ✔
   │           └─ With wrong Password ✔
   └─ ContractJUnit5 ✔
      └─ testHeaderForAPIVersion() ✔
         └─ Requests should contain header X-Broker-API-Version ✔
            ├─ GET - v2/catalog should reject with 412 ✔
            ├─ PUT - v2/service_instance/instance_id should reject with 412 ✔
            ├─ DELETE - v2/service_instance/instance_id should reject with 412 ✔
            ├─ GET - v2/service_instance/instance_id/last_operation should reject with 412 ✔
            ├─ DELETE - v2/service_instance/instance_id?service_id=Invalid&plan_id=Invalid  should reject with 412) ✔
            ├─ PUT - v2/service_instance/instance_id/service_binding/binding_id  should reject with 412) ✔
            └─ DELETE - v2/service_instance/instance_id/service_binding/binding_id?service_id=Invalid&plan_id=Invalid should reject with 412 ✔

Test run finished after 799251 ms
[        37 containers found      ]
[         0 containers skipped    ]
[        37 containers started    ]
[         0 containers aborted    ]
[        37 containers successful ]
[         0 containers failed     ]
[        56 tests found           ]
[         0 tests skipped         ]
[        56 tests started         ]
[         0 tests aborted         ]
[        56 tests successful      ]
[         0 tests failed          ]

```